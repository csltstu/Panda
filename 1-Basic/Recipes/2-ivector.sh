#!/bin/bash
# Copyright 2013   Daniel Povey
#           2014   David Snyder
#           2018   Lantian Li
# this is the standard i-vector system.

. ./cmd.sh
. ./path.sh
set -e

stage=0
# number of components
cnum=2048
civ=400
clda=150

path=data/data_mfcc/fisher_train/fisher_5000   # data-dir of the training set, given fisher_5000 as an example.
data=`basename $path`

# UBM/T-matrix training
if [ $stage -le 0 ]; then
  # subset for fast training.
  utils/subset_data_dir.sh $path 1000 $path/${data}_1k
  utils/subset_data_dir.sh $path 2000 $path/${data}_2k

  # diag-UBM training
  sid/train_diag_ubm.sh --nj 10 --cmd "$train_cmd" $path/${data}_1k $cnum \
    exp_ivector/$data/diag_ubm_${cnum}
  # full-UBM training
  sid/train_full_ubm.sh --nj 10 --cmd "$train_cmd" $path/${data}_2k \
    exp_ivector/$data/diag_ubm_${cnum} exp_ivector/$data/full_ubm_${cnum}
  # T-matrix
  sid/train_ivector_extractor.sh --nj 10 --cmd "$train_cmd -l mem_free=2G" \
    --num-iters 6 --ivector_dim $civ exp_ivector/$data/full_ubm_${cnum}/final.ubm $path \
    exp_ivector/$data/extractor_${cnum}_${civ}
fi


# LDA/PLDA training
if [ $stage -le 1 ]; then
  # i-vector extraction on the training set.
  sid/extract_ivectors.sh --cmd "$train_cmd -l mem_free=2G" --nj 10 \
    exp_ivector/$data/extractor_${cnum}_${civ} $path \
    exp_ivector/$data/ivectors_${data}_${cnum}_${civ}
  
  # compute LDA transform
  ivector-compute-lda --dim=$clda  --total-covariance-factor=0.1 \
    "ark:ivector-normalize-length scp:exp_ivector/$data/ivectors_${data}_${cnum}_${civ}/ivector.scp ark:- |" \
    ark:$path/utt2spk \
    exp_ivector/${data}/ivectors_${data}_${cnum}_${civ}/transform_${clda}.mat

  # compute PLDA transform
  ivector-compute-plda ark:$path/spk2utt \
    "ark:ivector-normalize-length scp:exp_ivector/${data}/ivectors_${data}_${cnum}_${civ}/ivector.scp  ark:- |" \
    exp_ivector/$data/ivectors_${data}_${cnum}_${civ}/plda 2>exp_ivector/$data/ivectors_${data}_${cnum}_${civ}/log/plda.log
fi


# i-vector extraction on the evaluation set.
if [ $stage -le 2 ]; then
  path=data/data_mfcc/fisher_test
  for sub in train_all test_3 test_9 test_18 test_30; do
    sid/extract_ivectors.sh --cmd "$train_cmd -l mem_free=2G" --nj 10 \
      exp_ivector/$data/extractor_${cnum}_${civ} $path/$sub \
      exp_ivector/$data/ivectors_${sub}_${cnum}_${civ}
  done

  for sub in test_short_20 test_short_50 test_short_100; do
    sid/extract_ivectors_seg.sh --cmd "$train_cmd -l mem_free=2G" --nj 10 \
      exp_ivector/$data/extractor_${cnum}_${civ} $path/$sub \
      exp_ivector/$data/ivectors_${sub}_${cnum}_${civ}
  done
fi


# i-vector evaluation
if [ $stage -le 3 ]; then
  echo 30-20f
  local/evaluate_iv.sh exp_ivector/$data/ivectors_${data}_${cnum}_${civ} \
    exp_ivector/$data/ivectors_train_all_${cnum}_${civ} \
    exp_ivector/$data/ivectors_test_short_20_${cnum}_${civ} \
    data/trials/30-20f.trl

  echo 30-50f
  local/evaluate_iv.sh exp_ivector/$data/ivectors_${data}_${cnum}_${civ} \
    exp_ivector/$data/ivectors_train_all_${cnum}_${civ} \
    exp_ivector/$data/ivectors_test_short_50_${cnum}_${civ} \
    data/trials/30-50f.trl

  echo 30-100f
  local/evaluate_iv.sh exp_ivector/$data/ivectors_${data}_${cnum}_${civ} \
    exp_ivector/$data/ivectors_train_all_${cnum}_${civ} \
    exp_ivector/$data/ivectors_test_short_100_${cnum}_${civ} \
    data/trials/30-100f.trl

  echo 30-3
  local/evaluate_iv.sh exp_ivector/$data/ivectors_${data}_${cnum}_${civ} \
    exp_ivector/$data/ivectors_train_all_${cnum}_${civ} \
    exp_ivector/$data/ivectors_test_3_${cnum}_${civ} \
    data/trials/30-3.trl

  echo 30-9
  local/evaluate_iv.sh exp_ivector/$data/ivectors_${data}_${cnum}_${civ} \
    exp_ivector/$data/ivectors_train_all_${cnum}_${civ} \
    exp_ivector/$data/ivectors_test_9_${cnum}_${civ} \
    data/trials/30-9.trl

  echo 30-18
  local/evaluate_iv.sh exp_ivector/$data/ivectors_${data}_${cnum}_${civ} \
    exp_ivector/$data/ivectors_train_all_${cnum}_${civ} \
    exp_ivector/$data/ivectors_test_18_${cnum}_${civ} \
    data/trials/30-18.trl

  echo 30-30
  local/evaluate_iv.sh exp_ivector/$data/ivectors_${data}_${cnum}_${civ} \
    exp_ivector/$data/ivectors_train_all_${cnum}_${civ} \
    exp_ivector/$data/ivectors_test_30_${cnum}_${civ} \
    data/trials/30-30.trl
fi

echo Done.