#!/bin/bash
# Copyright 2013   Daniel Povey
#           2018   Lantian Li
# this is the standard d-vector system, built in nnet3; it's what we use to embed deep speaker vector.

stage=0

. ./cmd.sh
. ./path.sh

data=fisher_5000   # name of the training set, given fisher_5000 as an example.
pnorm_input=2000   # dim of pnorm input.
pnorm_output=400   # dim of pnorm output.
dir=exp_ctdnn/$data/nnet3/cnn_4_8_pi${pnorm_input}_po${pnorm_output}_l6_splice10   # dir of CT-DNN models.

# d-vector extraction on the evaluation set.
if [ $stage -le 0 ]; then
  lang=data/data_fbank/chinese
  for sub in train-ch test-ch ; do
    sid/extract_dvectors.sh --nj 20 --cmd "$train_cmd" $dir $lang/$sub $dir/$sub
  done

  lang=data/data_fbank/uyghur
  for sub in train-uy test-uy ; do
    sid/extract_dvectors.sh --nj 20 --cmd "$train_cmd" $dir $lang/$sub $dir/$sub
  done
fi

# d-vector evaluation

### long trials
if [ $stage -le 1 ]; then
  echo Long-Ch-Ch
  local/evaluate_dv.sh $dir/$data \
    $dir/train-ch \
    $dir/test-ch \
    data/trials/long/ch-ch.trials

  echo Long-Uy-Uy
  local/evaluate_dv.sh $dir/$data \
    $dir/train-uy \
    $dir/test-uy \
    data/trials/long/uy-uy.trials

  echo Long-Ch-Uy
  local/evaluate_dv.sh $dir/$data \
    $dir/train-ch \
    $dir/test-uy \
    data/trials/long/ch-uy.trials

  echo Long-Uy-Ch
  local/evaluate_dv.sh $dir/$data \
    $dir/train-uy \
    $dir/test-ch \
    data/trials/long/uy-ch.trials
fi

### short trials
if [ $stage -le 2 ]; then
  echo Short-Ch-Ch
  local/evaluate_dv.sh $dir/$data \
    $dir/test-ch \
    $dir/test-ch \
    data/trials/short/ch-ch.trials

  echo Short-Uy-Uy
  local/evaluate_dv.sh $dir/$data \
    $dir/test-uy \
    $dir/test-uy \
    data/trials/short/uy-uy.trials

  echo Short-Ch-Uy
  local/evaluate_dv.sh $dir/$data \
    $dir/test-ch \
    $dir/test-uy \
    data/trials/short/ch-uy.trials
fi

echo Done.
