#!/bin/bash
# Copyright 2013   Daniel Povey
#           2018   Lantian Li
# this is the standard d-vector system, built in nnet3; it's what we use to embed deep speaker vector.

stage=0

. ./cmd.sh
. ./path.sh

data=fisher_5000   # name of the training set, given fisher_5000 as an example.
pnorm_input=2000   # dim of pnorm input.
pnorm_output=400   # dim of pnorm output.
dir=exp_ctdnn/$data/nnet3/cnn_4_8_pi${pnorm_input}_po${pnorm_output}_l6_splice10   # dir of CT-DNN models.

# d-vector extraction on CSLT-DISGUISE-I
if [ $stage -le 0 ]; then
  event=data/CSLT-DISGUISE-I/data_fbank
  name=disguise
  echo $name
  sid/extract_dvectors.sh --nj 20 --cmd "$train_cmd" $dir $event/$name $dir/$name
fi

# d-vector evaluation on CSLT-DISGUISE-I
if [ $stage -le 1 ]; then
  echo disguise
  local/evaluate_dv.sh $dir/$data \
    $dir/disguise \
    $dir/disguise \
    data/CSLT-DISGUISE-I/trials/disguise.trl
fi

# d-vector extraction on CSLT-TRIVIAL-I
if [ $stage -le 2 ]; then
  event=data/CSLT-TRIVIAL-I/data_fbank
  for name in akem cough hmm laugh sniff zeze; do
    echo $name
    sid/extract_dvectors.sh --nj 20 --cmd "$train_cmd" $dir $event/$name $dir/$name
  done
fi

# d-vector evaluation on CSLT-TRIVIAL-I
if [ $stage -le 3 ]; then
  event=data/CSLT-TRIVIAL-I/data_fbank
  for name in akem cough hmm laugh sniff zeze; do
    echo $name
    local/evaluate_dv.sh $dir/$data \
      $dir/$name \
      $dir/$name \
      data/CSLT-TRIVIAL-I/trials/${name}.trl
  done
fi

echo Done.
