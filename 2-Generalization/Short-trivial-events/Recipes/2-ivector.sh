#!/bin/bash
# Copyright 2013   Daniel Povey
#           2014   David Snyder
#           2018   Lantian Li
# this is the standard i-vector system.

. ./cmd.sh
. ./path.sh
set -e

stage=0
# number of components
cnum=2048
civ=400
clda=150
data=fisher_5000

# i-vector extraction on CSLT-DISGUISE-I
if [ $stage -le 0 ]; then
  event=data/CSLT-DISGUISE-I/data_mfcc
  name=disguise
  echo $name
  sid/extract_ivectors.sh --cmd "$train_cmd -l mem_free=2G" --nj 10 \
    exp_ivector/$data/extractor_${cnum}_${civ} $event/$name \
    exp_ivector/$data/ivectors_${name}_${cnum}_${civ}
fi

# i-vector evaluation on CSLT-DISGUISE-I
if [ $stage -le 1 ]; then
  echo disguise
  local/evaluate_iv.sh exp_ivector/$data/ivectors_${data}_${cnum}_${civ} \
    exp_ivector/$data/ivectors_disguise_${cnum}_${civ} \
    exp_ivector/$data/ivectors_disguise_${cnum}_${civ} \
    data/CSLT-DISGUISE-I/trials/disguise.trl
fi

# i-vector extraction on CSLT-TRIVIAL-I
if [ $stage -le 2 ]; then
  event=data/CSLT-TRIVIAL-I/data_mfcc
  for name in akem cough hmm laugh sniff zeze; do
    echo $name
    sid/extract_ivectors.sh --cmd "$train_cmd -l mem_free=2G" --nj 10 \
      exp_ivector/$data/extractor_${cnum}_${civ} $event/$name \
      exp_ivector/$data/ivectors_${name}_${cnum}_${civ}
  done
fi

# i-vector evaluation on CSLT-TRIVIAL-I
if [ $stage -le 3 ]; then
  for name in akem cough hmm laugh sniff zeze; do
    echo $name
    local/evaluate_iv.sh exp_ivector/$data/ivectors_${data}_${cnum}_${civ} \
      exp_ivector/$data/ivectors_${name}_${cnum}_${civ} \
      exp_ivector/$data/ivectors_${name}_${cnum}_${civ} \
      data/CSLT-TRIVIAL-I/trials/${name}.trl
  done
fi

echo Done.