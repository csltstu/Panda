#!/bin/bash
# Copyright 2013   Daniel Povey
#           2014   David Snyder
#           2018   Lantian Li

. ./cmd.sh
. ./path.sh
stage=0

set -e
mfccdir=`pwd`/_mfcc
vaddir=`pwd`/_vad
fbankdir=`pwd`/_fbank

# feature extraction on CSLT-DISGUISE-I
if [ $stage -le 0 ]; then
  data=data/CSLT-DISGUISE-I/data_mfcc/disguise
  # Make MFCCs.
  steps/make_mfcc.sh --mfcc-config conf/mfcc.conf --nj 20 --cmd "$train_cmd" \
    $data exp_ivector/_log_mfcc $mfccdir
  # Make energy-vad.
  sid/compute_vad_decision.sh --nj 10 --cmd "$train_cmd" \
    $data exp_ivector/_log_vad $vaddir

  data=data/CSLT-DISGUISE-I/data_fbank/disguise
  # Make Fbanks.
  steps/make_fbank.sh --fbank-config conf/fbank.conf --nj 20 --cmd "$train_cmd" \
    $data exp_ctdnn/_log_fbank $fbankdir
  # Make CMVN.
  steps/compute_cmvn_stats.sh $data $data/_cmvn $data/local/cmvn
  cp data/CSLT-DISGUISE-I/data_mfcc/disguise/vad.scp data/CSLT-DISGUISE-I/data_fbank/disguise/vad.scp
fi

# feature extraction on CSLT-TRIVIAL-I
if [ $stage -le 1 ]; then
  for event in akem cough hmm laugh sniff zeze; do
    data=data/CSLT-TRIVIAL-I/data_mfcc/$event
    # Make MFCCs.
    steps/make_mfcc.sh --mfcc-config conf/mfcc.conf --nj 20 --cmd "$train_cmd" \
      $data exp_ivector/_log_mfcc $mfccdir
    # Make energy-vad.
    sid/compute_vad_decision.sh --nj 10 --cmd "$train_cmd" \
      $data exp_ivector/_log_vad $vaddir

    data=data/CSLT-TRIVIAL-I/data_fbank/$event
    # Make Fbanks.
    steps/make_fbank.sh --fbank-config conf/fbank.conf --nj 20 --cmd "$train_cmd" \
      $data exp_ctdnn/_log_fbank $fbankdir
    # Make CMVN.
    steps/compute_cmvn_stats.sh $data $data/_cmvn $data/local/cmvn
    cp data/CSLT-TRIVIAL-I/data_mfcc/$event/vad.scp data/CSLT-TRIVIAL-I/data_fbank/$event/vad.scp
  done  
fi

echo Done.