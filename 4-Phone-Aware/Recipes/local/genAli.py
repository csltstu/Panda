#!/usr/bin/python

# Copyright  2018  Lantian Li
# Apache 2.0.

# this script is used to convert vad.ark to ali.ark

import sys

if __name__ =="__main__":
	fin_vad = sys.argv[1]
	fout_ali = sys.argv[2]
	fin = open(fin_vad, 'r')
	lines = fin.readlines()
    fin.close()
	fout = open(fout_ali, 'w')
   	for line in lines:
       	parts = line.split()
		fout.write(parts[0] + ' ')
		for i in range(1, len(parts)):
			if parts[i] == '1':
				fout.write(parts[i] + ' ')
		fout.write('\n')
	fout.close()