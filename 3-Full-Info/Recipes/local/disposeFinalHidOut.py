import os
import sys
import numpy as np
import math
import re

def buildDict(filename):
    vectorNum = 0
    returnDict = {}
    f = open(filename)
    for line in f.readlines():
        tempList = line.split()
        returnDict[tempList[0]] = int(tempList[1])
        vectorNum = max(vectorNum, int(tempList[1]))
    f.close()
    return returnDict, vectorNum + 1


def disposeFinalHidOut(dictFile, dvectorFile, finalHidFile):
    vectorLen = 0
    vectorNum = 0    

    utt2Dict, vectorNum = buildDict(dictFile)
 
    numSum = np.zeros((vectorNum, 1))
    rpMatrix = np.zeros((0, 0))
    pattern = re.compile(r'(?P<name>(\w|-)*)\s*\[(?P<list>.*)\]')
    f = open(dvectorFile)
    for line in f.readlines():
        match = pattern.match(line)
        tempName = match.groupdict()['name']
        tempList = match.groupdict()['list'].split()
        tempList = [float(i) for i in tempList]

        if (len(rpMatrix) == 0):
            vectorLen = len(tempList)
            rpMatrix = np.zeros((vectorNum, vectorLen))
        
        indexNum = utt2Dict[tempName]
        rpMatrix[indexNum] = np.array(tempList) + rpMatrix[indexNum]
        numSum[indexNum] += 1
    f.close()

    for i in range(0, vectorNum):
        rpMatrix[i] = 1.0 * rpMatrix[i] / numSum[i]

    for i in range(0, vectorNum): 
        rpMatrix[i] = 20.0 * rpMatrix[i] / math.sqrt(sum(np.multiply(rpMatrix[i], rpMatrix[i])))

    f = open(finalHidFile)
    lines = f.readlines()
    f.close()
    f = open(finalHidFile, 'w')
    i = -1
    for line in lines:
        if (i >= 0 and i < vectorNum):
            if (i < vectorNum - 1):
                rpltemp = rpMatrix[i]
                rpltemp = '  ' + ' '.join([str(x) for x in rpltemp]) + '\n'
                f.write(rpltemp)
            else:
                rpltemp = rpMatrix[i]
                rpltemp = '  ' + ' '.join([str(x) for x in rpltemp]) +' ]\n'
                f.write(rpltemp)
            i += 1
        elif (line[0:len('<ComponentName> Final_affine')] == '<ComponentName> Final_affine'):
            f.write(line)
            i += 1
        else:
            f.write(line)
    f.close()

if __name__ == '__main__':
    
    fin_f = sys.argv[1]
    fin_d = sys.argv[2]
    fin_m = sys.argv[3]	
    disposeFinalHidOut(fin_f, fin_d, fin_m)

