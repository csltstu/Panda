\chapter{基于卷积-时延深度神经网络的说话人特征学习}
\label{cha:ct-dnn}

\section{本章引论}

近年来，随着深度学习技术的蓬勃发展，人们对深度学习的认知也在不断进步。
与传统“知识驱动”的特征设计方法不同，深度学习可以通过灵活的深层网络结构(例如，深度神经网络DNN)
自动地从原始数据中学习到与任务相关的特征。
原始数据经过层层处理，与任务相关的信息将被增强、保留，而与任务无关的信息将被削弱、移除。
相关研究表明~\cite{dahl2012context,hinton2012deep}，这种特征学习方法已经在语音识别中成功应用，
基于深度神经网络的特征学习所学到的特征对语音内容具有极强的代表性，
而对其它不确定性(如噪音、信道、说话人信息等)尤为鲁棒。

特征学习在语音识别中的成功促进了其在说话人识别中的研究。
最早，Variani等人~\cite{variani2014deep,li2015improved}将
基于深度神经网络的特征学习方法运用在文本相关的说话人识别中。
研究表明这些特征学习方法已在文本相关的说话人识别中取得了较为满意的性能表现，
验证了特征学习在说话人识别中的可行性。
尽管如此，与主流的概率统计方法相比，其系统性能仍相差甚远。

本章在上述研究工作的基础上，进一步深入地研究了基于深度神经网络的说话人特征学习。
为了设计一个合理的网络结构来实现说话人特征学习，
我们尝试将“知识驱动”与“数据驱动”结合起来，在模型设计时尽可能地引入一些与语音信号相关的先验知识，
使设计出的模型能够更好地从语音数据中学到更具有代表性的说话人特征。
为此，本章首先从语音信号自身出发，分析了语音信号在时频空间中的基本特性，以及这些基本特性对说话人信息的表征形式。
在此基础上，我们设计了一个包含卷积、时延和组归一化的卷积-时延深度神经网络(CT-DNN)；
最后，从定性和定量两个角度分析了所学说话人特征的区分能力和表征能力。

\section{语音信号特性分析}
\label{cha:signal}

\subsection{语音信号的基本特性}

语音信号是一种短时平稳信号，其兼有局部属性和动态属性两种基本特性。

所谓局部属性，是指语音信号在时频空间中具有明显重复的典型模式。
整个语谱可以认为是由这些典型模式通过变异、叠加、组合等各种操作而生成的。
例如，某个音素、某个词会在不同时刻重复出现；某些共振峰形态也会在不同人的不同频带上反复出现。
这些典型模式事实上是由人类的发音机理所决定的：发音器官总会在特定发音上产生特定模式；
不同说话人在特定发音上总会共享某些类似的发音模式，并以某种变异方式表达出来。
图~\ref{fig:share} 中呈现了语音信号在时域和频域上所共享的典型发音模式。
从不同说话人的语音中我们可以找到在时频空间上局部相似的片段，这类片段即为典型发音模式，
其反映了语音信号的局部属性。

\begin{figure}[H]
	\centering
	\includegraphics[width=1\linewidth]{share}
	\caption{语音信号的局部属性。
		其中，语音1和语音2分别来自两个不同的说话人。
		左图(a)反映了语音信号在\textbf{频域}上的一个共享的典型发音模式；
		右图(b)反映了语音信号在\textbf{时域}上的一个共享的典型发音模式。}
	\label{fig:share}
\end{figure}

所谓动态属性，是指语音信号具有时序相关性。某一时刻的发音状态会受前后发音状态的影响。
语音的动态性显然也与人的发音机理相关：发音器官无法在短时间内发生跳变，
因而产生的发音模式总会受上下文的影响。
图~\ref{fig:dynamic} 中呈现了语音信号在时频空间上的动态属性。

\begin{figure}[H]
	\centering
	\includegraphics[width=0.75\linewidth]{dynamic}
	\caption{语音信号的动态属性。
		同一发音模式因上下文发音状态的不同而有所不同。}
	\label{fig:dynamic}
\end{figure}

\subsection{说话人信息在语音信号中的表征形式}

对于语音信号的局部属性和动态属性，其也代表了说话人信息在语音信号中的两种表征形式。

对局部属性而言，不同说话人的相同发音(如同为`a'音)表现出各异的局部发音模式，因此发音模式本身
即包含了显著的说话人特性。即使两个说话人的发音模式完全一样，这些模式在时频空间中的分布特性
也是不同的，这些分布特性也是区分说话人的显著特征。因此，语音信号的局部属性(发音模式及其分布)具有
丰富的说话人信息。类比于笔迹鉴定，这相当于通过观察点、勾等局部形态
及其在整篇文字中的分布情况对书写者进行判定。

对动态属性而言，不同说话人对发音器官的支配方式具有各异性，而这种支配方式的差异体现在发音过程中，
特别是在音素、音节的过渡中表现得更为明显，
这表明说话人信息在发音过程中表现地更加突出。
显然，这种过程信息是“长时”的，只有通过较长的上下文才能捕获到。

\section{特征学习模型设计}

为了设计一个合理的网络结构来实现说话人特征学习，
本节尝试将“知识驱动”与“数据驱动”结合起来，
在模型设计时尽可能地引入语音信号中与说话人信息相关的先验知识，
使设计出的模型对说话人信息有着更好的表征能力。
为此，本节我们利用上述语音信号的基本特性，以及这些基本特性对说话人信息的表征形式，
设计了一个包含卷积、时延和组归一化的卷积-时延深度神经网络，
实现了对说话人信息中局部属性和动态属性的学习。

\subsection{卷积神经网络}

从语音信号的局部属性可知，语音信号在时域和频域上具有很强的结构化特性。
在时域结构上，时序相近的语音片段相关性很强；同一发音模式可能出现在同一个语音信号的不同时间片段中。
在频域结构上，语音信号中相近频段具有较强的相关性；同一发音模式可能在不同频段上重复出现。

针对语音信号在时频上的结构化特性，我们选择了具有结构化描述能力的卷积神经网络(CNN)。
CNN利用语音信号的结构化特性，设计出局部的、共享的网络子结构，每个子结构用于学习某种局部模式，
且在不同时域和频域位置的子结构共享网络参数。
图~\ref{fig:cnn} 给出了一个简单的CNN网络~\cite{lecun1989backpropagation}，
其中包括了一个卷积层和一个降采样层。

卷积层利用一个局部网络将某一时频位置的原始数据映射到特征空间的某一结点，且不同位置的局部网络是参数共享的。
这相当于利用了一个由该局部网络组成的卷积核对输入平面(语谱图)进行卷积操作，生成一个新的特征平面。
卷积核的作用类似于一个滤波器，可以用来学习时频空间中重复的典型模式。
为了提高网络的特征表征能力，CNN会通过多个卷积核生成多个特征平面，每个特征平面可学习时频空间中的某一方面特性。
降采样层则是利用一个简单的卷积核(如取平均或取最大值)对特征平面进行降维。
此外，降采样层还可以去除因语音信号的轻微变化而引起的特征抖动，提高模型的泛化能力。

\begin{figure}[H]
	\centering
	\includegraphics[width=0.75\linewidth]{cnn}
	\caption{卷积神经网络。注：卷积核在不同位置(红色和蓝色)进行卷积运算时，其参数是共享的。}
	\label{fig:cnn}
\end{figure}

\subsection{时延神经网络}

从语音信号的动态属性可知，语音信号具有时序相关性。同一发音模式会因上下文环境的不同而发生改变。
特别地，这种动态属性蕴含着丰富的说话人信息，使得说话人信息通常被认为具有长时性。
研究表明，在原始声学特征中加入一些时序动态特征
(一阶差分$\Delta$或二阶差分$\Delta\Delta$~\cite{furui1981cepstral,huang2001spoken,soong1988use})
将极大地提升说话人识别系统的性能。
对特征学习而言，为了满足说话人长时性的特点，应当设计一种对语音信号动态特性有着很好描述能力的网络结构。
为此，我们选择了对长时动态特性有着较强描述能力的时延神经网络(TDNN)~\cite{lecun1989backpropagation}。
图~\ref{fig:tdnn} 给出了本文所用的TDNN网络结构。

对于标准的深度神经网络DNN，其在处理长时的输入上下文时，网络的输入层需要覆盖全部的上下文信息。
与DNN不同的是，TDNN对长时上下文的描述并不受限于网络的输入层，
而是将时序的上下文信息放置于不同的隐藏层中。图~\ref{fig:tdnn} 中的第三层和第五层即为两个时延层。
在模型浅层，学习较窄的上下文信息(时域分辨率高)；而在模型深层，学习较宽的上下文信息(时域分辨率低)。
在图~\ref{fig:tdnn} 中，网络的第一个时延层(第三层)仅描述了前后各$2$帧之间的变化关系；
而网络的第二个时延层(第五层)则描述了前后各$4$帧之间的变化关系。
这种层次递进的学习策略与人脑处理信息的方式类似，
优先学习相对简单、局部的模式，而后再学习更为复杂、全局的模式。
研究表明，TDNN比DNN有着更好的长时描述能力~\cite{peddinti2015time}。

此外，循环神经网络(RNN)也具有对语音长时动态特性的描述能力，
但受限于其前后帧之间的从属关系，RNN模型难以实现网络的并行训练。
然而，TDNN很好了地继承了DNN前向反馈结构，
并且可通过在时域上的权值共享机制(相当于在时域上的一维CNN)实现网络的并行训练。
此外，为了进一步减少网络参数量、提高网络训练效率，
我们采用了帧采样机制~\cite{peddinti2015time}解决网络相邻结点之间上下文的重叠冗余。
如图~\ref{fig:tdnn}，灰色和红色连线是标准TDNN的网络训练路径；红色连接线代表了帧采样后的网络训练路径。
显然，通过帧采样机制，网络的训练复杂度将极大地降低。

\begin{figure}[H]
	\centering
	\includegraphics[width=0.85\linewidth]{tdnn}
	\caption{时延神经网络}
	\label{fig:tdnn}
\end{figure}

\subsection{基于$p$-范数的组归一化}

为了减小网络的训练规模、增强网络的可训练性，本文采用了基于“降维”的非线性激活函数。
首先对网络隐藏层中的神经元进行分组，而后采用$p$-范数作为非线性激活函数得到输出值。
例如，神经网络的隐藏层共有$2,000$个神经元，首先将这$2,000$个神经元每$5$个分为一组，每组神经元用$X$来表示；
而后每个组内的神经元进行$p$-范数计算得到该组神经元的输出$y$。$p$-范数的计算公式如下：

\begin{equation}
\label{equ:pnorm}
y = ||X||_{p} = \left( \sum_{i}|x_{i}|^p \right) ^{1/p}
\end{equation}

\noindent 其中，$p$是可调参数。依据~\inlinecite{zhang2014improving}的研究经验，在本文中我们设$p=2$。
在实验中，隐藏层中的每组神经元在经过$2$-范数的组归一化后，
我们又对所有组归一化后的各组输出值进行了长度归一化处理(本文采用基于$2$-范数的长度归一化)，
将最后隐藏层的激活输出规整到一个平滑的球面空间中，使网络训练更加稳定。

\subsection{CT-DNN模型结构}

综合卷积神经网络的局部属性学习能力、时延神经网络的动态属性描述能力和组归一化的模型可训练能力，
我们设计了一个基于卷积-时延深度神经网络(Convolutional time-delay deep neural network, CT-DNN)
的说话人特征学习模型，如图~\ref{fig:ct-dnn} 所示。

\begin{figure}[H]
	\centering
	\includegraphics[width=1\linewidth]{ct-dnn-raw}
	\caption{基于CT-DNN的说话人特征学习模型}
	\label{fig:ct-dnn}
\end{figure}

整个模型主要由卷积模块和时延模块两部分组成。
两个模块通过一个瓶颈层连接起来。
对于卷积模块，其由两个卷积层组成，在每个卷积层后经过一个降采样层。
卷积模块用于学习在语音信号时频空间中与说话人特性相关的局部模式。
对于时延模块，其共由五个隐藏层组成，每个隐藏层经过组归一化和长度归一化后得到隐藏层的激活输出。
在这五个隐藏层中，共有两个时延层。
时延模块用于扩大上下文的时序信息，更好地刻画语音信号的动态属性。
在最后一个隐藏层之后是一个全连接的分类层，其分类目标是训练集中的不同说话人。
显然，与~\inlinecite{variani2014deep,li2015improved}提出的DNN模型相比，
该CT-DNN模型更像是基于“知识驱动”的特征设计，尽管模型中的滤波器不再是人为设计的，
而是基于“数据驱动”从语音数据中自动学习得到的。

在模型训练中，以目标说话人和模型预测说话人之间的交叉熵为目标函数，
最大化地区分训练集中的不同说话人，并采用自然随机梯度下降(NSGD)~\cite{povey2014parallel}
算法实现对CT-DNN模型的训练。
由于最后一个隐藏层最接近于网络的训练目标，其具有更丰富的说话人区分性信息，
因此我们将最后一个隐藏层定义为特征提取层
\footnote{我们在实验中对比了各个隐藏层所输出的特征，结果显示最后一个隐藏层的输出特征性能最优。}。
该特征提取层经过组归一化和长度归一化后的输出即为“\textbf{说话人特征}”。

当模型训练完成后，我们便可从特征提取层中得到帧级别的说话人特征。
我们继承了~\inlinecite{variani2014deep,li2015improved}的后端处理方式，
将帧级别的说话人特征以合并平均的方式得到句子级别的表示，并称这种
句子级别的表示为\textbf{深度说话人向量}，简称为 \textbf{d-vector}
\footnote{复用了~\inlinecite{variani2014deep,li2015improved}的命名方式。} 。
在识别测试时，首先分别计算测试语音和预留语音的 d-vectors ；
然后通过计算两个 d-vectors 之间的余弦距离即可得到最终的判决打分。
与主流的概率统计模型 i-vector ~\cite{dehak2011front}类似，
可以通过引入一些正则化方法(如，线性判别分析LDA~\cite{dehak2011front}、
概率线性判别分析PLDA~\cite{kenny2010bayesian,ioffe2006probabilistic}等)，
进一步提高 d-vector 的说话人区分性。
我们称这个基于d-vector的说话人识别系统为\textbf{d-vector系统}。

\section{实验}
\label{cha:exp-basic}

本节我们首先将介绍实验所选用的数据库和相关模型配置，
而后给出 i-vector基线系统 和 d-vector 系统在不同测试时长下的识别性能，
最后从多个角度分析 i-vector 模型 和 d-vector 模型之间的差异。

\subsection{实验数据}
\label{cha:data}

在本实验中，我们选用标准的英文电话信道语料库 \emph{Fisher}~\cite{fisher1,fisher2}，
其语音数据的采样率为8kHz，采样精度为16bits。
训练集和测试集的详细信息如下：

\begin{itemize}
	
	\item \textbf{训练集}: 从 \emph{Fisher} 数据库中随机选取了$95,167$个语音段，共覆盖了
	$5,000$个说话人，其中$2,500$个男性和$2,500$个女性。
	每个说话人的语音时长约$120$秒。
	对 i-vector 系统而言，该数据集用于训练 UBM、T 矩阵以及基于 i-vector 的LDA、PLDA模型。
	对 d-vector 系统而言，该数据集用于训练 CT-DNN 模型以及基于 d-vector 的LDA、PLDA模型。
	\item \textbf{测试集}: 从 \emph{Fisher} 数据库中随机选取$500$个男性和$500$个女性说话人。
	对于每个说话人，随机选取$10$条语音段(时长约$30$秒)用于说话人预留，其余语音用于说话人测试。
	注：训练集和测试集中的说话人之间没有任何交集。
	
\end{itemize}

\subsection{系统配置}
\label{cha:config}

在本实验中，我们选取 GMM i-vector 系统作为基线。
i-vector系统所选用的声学特征共$60$维，其中包含了$19$维梅尔频率倒谱系数MFCCs和
$1$维对数能量以及这些特征的一阶差分和二阶差分。
考虑到语音信号为 $8$kHz 采样的电话信道语音，根据 Nyquist 采样定理，
所处理的频率范围设为 $20$Hz 至 $3,700$Hz。
为了增强特征的有效性，我们对特征采取了基于能量的语音活动检测(VAD)。
通用背景模型UBM的高斯混合数设为$2,048$，因此对应的高斯均值超向量的维度为$2,048*60$。
描述全变量空间的 T 矩阵将高维的高斯超向量映射到低维的说话人子空间(i-vector)中；
其中，i-vector的维度为$400$。
此外，LDA降维后的映射空间为$150$维；在PLDA打分之前，i-vectors将经过中心化和长度归一化等预处理。
整个i-vector基线系统是基于Kaldi~\cite{povey2011kaldi} SRE08/SRE10 实现的。

对于d-vector系统，其CT-DNN模型结构如图~\ref{fig:ct-dnn} 所示。
d-vector系统所选用的声学特征为$40$维的Fbanks特征；
在此基础上，拼接前$4$帧和后$4$帧总计$9$帧构成了$9*40$维的特征矩阵，
作为卷积模块的输入。
卷积模块的参数配置如表~\ref{tab:cnn} 所示。

\begin{table}[htp]
	\begin{center}
		\caption{卷积模块的参数配置}
		\label{tab:cnn}
		\resizebox{1\linewidth}{!}{
		\begin{tabular}{l|p{4cm}<{\centering}|p{2.5cm}<{\centering}|
				p{2.5cm}<{\centering}|p{2.5cm}<{\centering}|p{2.5cm}<{\centering}}
			\hline
			卷积层 C1  & 输入特征 & 滤波器大小 & 滤波器个数 &  移动步长  & 输出特征  \\
			          &   1@9*40    &    4*8     &   128     &   1*1     &    128@6*33   \\
			\hline
			\hline
			降采样层 P1    & 输入特征 & 采样大小   &  移动步长 & 输出特征   \\
                       &   128@6*33  &     2*3    &    2*3   &   128@3*11  \\
			\hline
			\hline
			卷积层 C2    & 输入特征 & 滤波器大小 & 滤波器个数 &  移动步长  & 输出特征  \\
			            &   128@3*11  &    2*4     &   256     &     1*1    &   256@2*8   \\
			\hline
			\hline
			降采样层 P2  & 输入特征 & 采样大小 &  移动步长 & 输出特征  \\
			           &   256@2*8   &     1*2     &    1*2   &   256@2*4  \\
		\hline
		\end{tabular}
	}
	\end{center}
\end{table}

卷积模块之后是一个包含$512$个隐藏结点的瓶颈层，其用于连接卷积模块和时延模块。
整个时延模块共由五个隐藏层组成，每个隐藏层有$2,000$个结点。
对于每个隐藏层，首先有序地将每$5$个不交叠的结点进行基于$2$-范数的组归一化，
使原始$2,000$维的隐藏层输出降至$400$维；
随后$400$维的输出向量经过长度归一化得到该隐藏层的激活输出，并作为下一层的输入。
时延模块的第二层和第四层是两个时延层，其所对应的时延分别是当前帧的前后各$2$帧和前后各$4$帧。
考虑上卷积模块输入特征的上下文信息，整个CT-DNN模型的有效输入共计$21$帧。
最后一个隐藏层在组归一化、长度归一化后的$400$维输出视为\textbf{说话人特征}。
网络输出层的结点个数即为训练集中的说话人个数，取值为$5,000$。
通过合并平均的方式，将帧级别的说话人特征变成句子级别的表示(d-vector)。
与 i-vector类似，我们可以选择简单的余弦距离作为d-vectors之间的度量准则，
当然也可以采用其它相关的打分度量方法(如PLDA打分)。

\subsection{定性分析}

在将基于CT-DNN模型所学到的说话人特征应用于不同说话人识别任务之前，
我们首先通过可视化的方式，对所学特征进行定性分析，评估所学特征的区分性能力。

\subsubsection{说话人图谱}

首先，我们绘制了某个语音片段中说话人特征的灰度图。在这个灰度图中，颜色越亮的地方代表数值越大。
我们称该灰度图为\textbf{说话人图谱}，其描述了一段语音中说话人特征的静态属性和动态属性。
为了更清晰地展示，我们采用\emph{Roberts}算子~\cite{roberts1963machine} 对说话人图谱进行边缘锐化。
图~\ref{fig:spkgram} 给出了来自三个不同说话人的三个语音片段的说话人图谱。
在这些说话人图谱中，尽管有部分噪音的存在，但是每个语音片段中的说话人特征在某些维度上的分布具有一定的稳定性；
而且不同说话人的说话人图谱之间存在着明显的差异。

\begin{figure}[H]
	\centering
	\includegraphics[width=1\linewidth]{spkgram}
	\caption{三个不同说话人的说话人图谱}
	\label{fig:spkgram}
\end{figure}

\subsubsection{t-SNE}

为了更好地表征说话人特征的区分能力，我们尝试将$400$维的说话人特征映射到一个二维空间中，
更清晰地实现对说话人特征的可视化。

t-分布邻域嵌入算法(t-SNE)~\cite{maaten2008visualizing}
是一种基于概率的局部非线性流形学习方法，其基本思想是：若要使高维数据映射到低维空间后的拓扑结构保持不变，
则在高维空间中相似的数据点在低维空间中应保持其相似性。
本文选用t-SNE方法，通过数据降维后的可视化，完成对原始说话人特征的拓扑分析。

图~\ref{fig:phone-blind} 为所学说话人特征经t-SNE降维后的二维分布图。
其中，图中的特征来自测试集中的$20$个说话人，每种颜色代表着一个说话人。
此外，左右两幅图采用不同的特征选择方法。
在图~\ref{fig:phone-blind} (a)中，每个说话人的特征是从每个说话人的不同语音片段中随机采样的；
在图~\ref{fig:phone-blind} (b)中，每个说话人的特征取自于每个说话人的某一个连续的语音片段。
从两幅图中可以看出，所学说话人特征具有很强的说话人区分性，并且来自同一个说话人的特征又具有较强的内聚性。
相较于原始声学特征(如Fbanks、MFCC)，所学说话人特征对说话人特性的描述能力有了极大的提升，
这也意味着我们只需一个简单的后端模型即可完成对不同说话人的区分。
此外，从图~\ref{fig:phone-blind} (b)中我们还观察到，该说话人特征在连续语音片段中具有与文本内容相关的模式，
这表明所学说话人特征中隐含着部分发音内容信息。
这并不令人奇怪，因为说话人特性本来就附属于不同发音模式的语音信号中。
当然，为了更好地去除发音内容对说话人特征的扰动，在后续的章节中我们将开展相关研究。

\begin{figure}[H]
	\centering
	\includegraphics[width=0.90\linewidth]{phone-blind}
	\caption{基于t-SNE的说话人特征可视化}
	\label{fig:phone-blind}
\end{figure}


\subsection{定量分析}
\label{cha:quantity}

本节从定量的角度验证所学说话人特征的区分性。
我们将所学到的说话人特征应用于不同的说话人识别任务(包括说话人确认和说话人辨认)和测试场景(包括短时场景和长时场景)中，
并比较i-vector基线系统和d-vector系统的性能表现。

\subsubsection{说话人确认}
\label{cha:verification}

1. \textbf{测试场景}

为了更好地对比 i-vector 和 d-vector 系统，我们设计了两种测试场景：长时场景和短时场景。
在长时场景中，测试语音的时长分别为$3$秒、$9$秒和$18$秒；
在短时场景中，测试语音的时长分别为$21$帧($0.3$秒)、$51$帧($0.6$秒)和$101$帧($1.2$秒)。
在两种测试场景中，每个说话人的建模语音时长约为$30$秒。
短时场景和长时场景的测试配置详见表~\ref{tab:data-short} 和表~\ref{tab:data-long}。
所有的测试场景/条件均采用性别相关的测试列表。
值得注意的是，由于整个CT-DNN模型的有效输入特征共计$21$帧，因此在S(30-21f)的测试条件下，从测试语音中仅能提取一帧有效的说话人特征。

\begin{table}[htp]
	\begin{center}
		\caption{短时场景下的测试配置}
		\label{tab:data-short}
		\resizebox{0.68\linewidth}{!}{
		\begin{tabular}{|l|p{2cm}<{\centering}|p{2cm}<{\centering}|p{2cm}<{\centering}|}
			\hline
			\multirow{2}*{\diagbox[width=8em]{数据配置}{测试场景}}   &  \multicolumn{3}{c|}{短时场景}  \\\cline{2-4}
			                 &   S(30-21f) &    S(30-51f)   &   S(30-101f)  \\
			\hline
			预留语音(条)      &   10k      &    10k       &    10k         \\
			测试语音(条)      &   73k      &    73k       &    73k        \\
			\hline
			预留时长(秒)      &   30       &    30        &    30         \\
			测试时长(秒)      &   0.3      &    0.6       &    1.2         \\
			\hline
			真实测试(次)      &   73k      &    73k       &    73k          \\
			闯入测试(次)      &   36M      &    36M       &    36M        \\
			\hline
		\end{tabular}
		}
	\end{center}
\end{table}


\begin{table}[htp]
	\begin{center}
		\caption{长时场景下的测试配置}
		\label{tab:data-long}
		\resizebox{0.68\linewidth}{!}{
			\begin{tabular}{|l|p{2cm}<{\centering}|p{2cm}<{\centering}|p{2cm}<{\centering}|}
				\hline
				\multirow{2}*{\diagbox[width=8em]{数据配置}{测试场景}}   &  \multicolumn{3}{c|}{长时场景} \\\cline{2-4}
				 &   L(30-3) &    L(30-9)   &   L(30-18) \\
				\hline
				预留语音(条)      &   10k     &    10k       &    10k       \\
				测试语音(条)      &   73k     &    24k       &    12k       \\
				\hline
				预留时长(秒)      &   30      &    30       &     30        \\
				测试时长(秒)      &   3       &    9        &     18        \\
				\hline
				真实测试(次)      &   73k     &    24k       &     12k      \\
				闯入测试(次)      &   36M     &    12M       &     6M        \\
				\hline
			\end{tabular}
		}
	\end{center}
\end{table}

2. \textbf{系统配置}

在测试阶段，每条语音的i-vector和d-vector分别从i-vector系统和d-vector系统中提取出来。
通过计算预留语音和测试语音i-vectors/d-vectors之间的相似度进行打分判决。
对于两个系统，我们分别采用了三种打分策略：(1) 基于原始$400$维向量的余弦距离；
(2) 基于LDA变换后$150$维向量的余弦距离；
(3) 原始$400$维向量经中心化和长度归一化后的PLDA打分~\cite{ioffe2006probabilistic}。
此外，我们选用等错误率(EER)作为系统性能的评价指标。

在训练LDA和PLDA模型时，我们首先把训练集中的语音分割成$3$秒左右的片段；
然后提取这些语音片段对应的i-vectors和d-vectors；
最后基于这些i-vectors和d-vectors，分别训练i-vector系统和d-vector系统的LDA和PLDA模型。
值得注意的是，这种训练方式可能会导致LDA和PLDA模型在短时场景上取得更好地效果，
但从实验角度看，这并不是一个很大的问题。
一来相比于长时场景，短时场景在实际应用中更加重要(短时具有更好的体验性)；
二来在短时场景上的偏向对于i-vector系统和d-vector系统的影响是等同的，
因此并不会影响我们对这两个系统的比较。

3. \textbf{实验结果}

首先对于短时场景，我们从表~\ref{tab:result-sv-short} 中可以看出，
在三种短时测试条件下，d-vector系统的最优性能均远好于
i-vector系统的最优性能。
尽管通过LDA和PLDA模型提升了原始i-vector模型的区分性，但其在短时场景下的结果仍难以令人满意。
而对于d-vector系统，其采用最简单的Cosine打分度量方法即可取得不俗的性能表现。
尤其是在S(30-21f)测试条件下，虽仅有一帧(约0.3秒)有效说话人特征，但d-vector系统的等错误率(EER)达到了8.31\%。
与之相比，i-vector系统在此条件下的等错误率(EER)约30\%。
这些实验结果表明所学到的说话人特征具有极强的说话人区分性。

随着测试语音时长的增大，如表~\ref{tab:result-sv-long} 所示，
i-vector和d-vector系统的性能均得到了明显的提升，但i-vector系统的提升程度更为显著。
当测试语音的时长超过9秒时，i-vector系统的最优性能(0.88\%)已超越了d-vector系统的最优性能(1.48\%)。
当然，这个现象也是可以理解的。
i-vector系统是基于概率统计模型来预测说话人因子的，而长时语音可以增强i-vector预测过程的可靠性。
与之相反，d-vector系统更关注于短时(帧级别)的说话人区分性，
句子级别的说话人表示(d-vector)是通过一个合并平均的后端模型得到，
而这一简单的后端模型限制了d-vector对长时语音的处理能力。

\begin{table}[htb]
	\begin{center}
		\caption{短时测试场景下的说话人确认识别结果}
		\label{tab:result-sv-short}
		\resizebox{0.75\linewidth}{!}{
		\begin{tabular}{|c|l|p{2cm}<{\centering}|p{2cm}<{\centering}|p{2cm}<{\centering}|}
			\hline
			测试系统       &  打分度量 & \multicolumn{3}{c|}{短时场景EER(\%)}  \\\cline{3-5}
			%\hline
			              &          & S(30-21f) & S(30-51f) &  S(30-101f)   \\
			\hline
			i-vector      &    Cosine   &  30.01    &  18.23    &  11.14   \\
			&    LDA      &  29.47    &  15.96    &   8.64      \\
			&    PLDA     &  29.29    &  15.71    &   8.34      \\
			\hline
			d-vector           &  Cosine     & \textbf{8.31} & 7.09  &   4.77   \\
			&    LDA      &   8.48    & \textbf{4.92} & \textbf{3.02}    \\
			&    PLDA     &  24.63    &  17.47    &  10.45     \\
			\hline
		\end{tabular}
		}
	\end{center}
\end{table}

\begin{table}[htb]
	\begin{center}
		\caption{长时测试场景下的说话人确认识别结果}
		\label{tab:result-sv-long}
		\resizebox{0.75\linewidth}{!}{
			\begin{tabular}{|c|l|p{2cm}<{\centering}|p{2cm}<{\centering}|p{2cm}<{\centering}|}
				\hline
				测试系统       &  打分度量 & \multicolumn{3}{c|}{长时场景EER(\%)} \\\cline{3-5}
				%\hline
				&          &  L(30-3) & L(30-9) & L(30-18)  \\
				\hline
				i-vector      &    Cosine   &   3.77   &  1.09   &\textbf{0.53}   \\
				&    LDA      &   3.11   &  1.01   &    0.63   \\
				&    PLDA     &   3.04   &\textbf{0.88} &  0.57  \\
				\hline
				d-vector      &  Cosine   &   3.79   &  2.56  &  2.30  \\
				&    LDA      & \textbf{2.13} &  1.48  &  1.33    \\
				&    PLDA     &   7.96    &  4.06  &  3.59  \\
				\hline
			\end{tabular}
		}
	\end{center}
\end{table}

通过比较三种打分度量方法可以看出，对于d-vector系统，LDA打分度量方法比最基础的Cosine打分度量方法的效果更好。
这给人的第一印象似乎并不合理，因为CT-DNN的模型训练本来就是区分性的，其应该取代了LDA模型的功能。
但仔细地分析，我们发现每个说话人特征空间的分布情况是不同的，这就导致不同说话人的类内离散度存在差异，
而这种差异使得说话人确认系统难以得到一个相对稳定的阈值。
LDA模型提供了一个简单的线性变换，通过对不同说话人类内与类间的统一限制，
实现了对不同说话人的类内空间的归一化。
显然，LDA模型对说话人类内离散度的限制是特征学习模型所不具备的。
从这个角度来看，LDA模型可作为d-vector系统的后端模型，进一步提高说话人确认系统的性能。
Heigold等人~\cite{heigold2016end}也有类似的发现。
他们在文本相关的说话人确认任务中，通过分数归一化(T-norm)的方式提高了d-vector系统的性能。
本质上，T-norm和LDA模型对说话人类内归一化有着类似的作用。

PLDA模型也具有与LDA模型相似的说话人类内归一化的作用，但是在我们的实验中PLDA打分度量方法并未有效地提高识别性能。
一个可能原因是，每个说话人的d-vector均值向量并不符合高斯先验假设，所以d-vector不能被PLDA模型很好地建模。
为了验证这个猜想，我们计算了 d-vector模型 的 Kurtosis 和 Skewness 分布。为了更好地对比，
我们也同样计算了 i-vector模型 的 Kurtosis 和 Skewness 分布。
Kurtosis 和 Skewness 分布的定义可参见：
\footnote{https://en.wikipedia.org/wiki/Kurtosis}\footnote{https://en.wikipedia.org/wiki/Skewness}
，其计算公式如下：

\vspace{-1mm}

\begin{equation}
\label{equ:kurt}
Kurt(y)=\frac{E[(y-\mu_y)^4]}{\sigma_y^4} - 3
\end{equation}

\vspace{-3mm}

\begin{equation}
\label{equ:skew}
Skew(y) = \frac {E[(y-\mu_y)^3]}{\sigma_y^3}
\end{equation}

\noindent 其中，$\mu_y$ 和 $\sigma_y$ 分别代表变量 $y$ 的均值和标准差。
若变量$y$的分布越高斯，则$Kurt(y)$和$Skew(y)$的数值越趋近于0。
表~\ref{tab:utt-kurt-1} 给出了句子级别的 i-vector 和 d-vector 的 $Kurt(y)$ 和 $Skew(y)$ 值；
表~\ref{tab:spk-kurt-1} 给出了说话人级别的 i-vector 和 d-vector 的 $Kurt(y)$ 和 $Skew(y)$ 值。
其中，说话人级别是指每个说话人所有句子级别 i-vectors 和 d-vectors 的平均。

\begin{table}[htp]
	\begin{center}
		\caption{句子级别 i-vector 和 d-vector 的 $Kurt(y)$ 和 $Skew(y)$ 值}
		\label{tab:utt-kurt-1}
		\resizebox{0.48\linewidth}{!}{
			\begin{tabular}{|p{2cm}<{\centering}|p{2cm}<{\centering}|p{2cm}<{\centering}|}
				\hline
				模型   &  Kurtosis  &  Skewness   \\
				\hline
				\hline
				i-vector   &   0.00067   &  0.16172   \\
				\hline
				d-vector   &   1.92076   &  5.66093   \\
				\hline
			\end{tabular}
		}
	\end{center}
\end{table}


\begin{table}[htp]
	\begin{center}
		\caption{说话人级别 i-vector 和 d-vector 的 $Kurt(y)$ 和 $Skew(y)$ 值}
		\label{tab:spk-kurt-1}
		\resizebox{0.48\linewidth}{!}{
			\begin{tabular}{|p{2cm}<{\centering}|p{2cm}<{\centering}|p{2cm}<{\centering}|}
				\hline
				模型    &  Kurtosis  &  Skewness   \\
				\hline
				\hline
				i-vector   &   -0.00424  &  0.00841   \\
				\hline
				d-vector   &   1.95720   &  5.89390   \\
				\hline
			\end{tabular}
		}
	\end{center}
\end{table}

从两组实验结果可以看出，对于i-vector模型，无论是句子级别还是说话人级别，其呈现出明显的高斯性。
因此，i-vector模型完美地符合PLDA模型的高斯假设。
而对于d-vector模型，无论是句子级别还是说话人级别，其呈现出明显的非高斯性。
显然，d-vector模型不符合PLDA模型的高斯假设，因此其PLDA打分结果并不理想。

此外，我们还注意到当测试语音时长为$18$秒时，LDA和PLDA打分度量方法并没有有效地提升i-vector系统的性能。
这是因为LDA和PLDA模型是在短语音(3秒)条件下训练的，与长语音测试条件并不匹配。

从以上结果可以看出，与i-vector基线系统相比，d-vector系统在短时场景中展现出了明显的优势。
这种优势归因于说话人特征学习的训练方法，其通过帧级别的区分性训练，得到帧级别的、强区分性的说话人特征，
而这是概率统计模型所无法比拟的。

\subsubsection{说话人辨认}

本小节我们将所学到的说话人特征应用到闭集的说话人辨认任务中。
与~\ref{cha:verification} 节的说话人确认任务不同，
说话人辨认是判定待测试语音来自于目标说话人模型库中的哪一个人，是“多对一”的选择问题。
因此，说话人辨认不需要一个全局判决阈值，其重点是不同说话人之间的相对分数。

1. \textbf{系统配置}

在本实验中，我们构建了一个与说话人确认任务相同的i-vector基线系统和d-vector系统。
在测试过程中，首先每条测试语音的i-vector和d-vector分别从i-vector系统和d-vector系统中提取出来；
然后测试语音的i-vector/d-vector将和所有预留说话人的i-vectors/d-vectors模型进行打分度量。
其中，打分度量方法与说话人确认任务相同，依然是Cosine，LDA和PLDA。
最后，我们采用Top-1辨认正确率(Top-1 IDR)来评估两个系统的辨认性能。
这里的Top-1 IDR是指与待测试语音最相似的说话人恰好是目标说话人的测试数占总测试语音数的比例。

2. \textbf{实验结果}

首先针对短时场景，我们从表~\ref{tab:result-sid-short} 可以看出，
在三种短时测试条件下，d-vector系统的最优性能均远好于i-vector系统的最优性能。
尤其是在S(30-21f)测试条件下，虽仅有一帧(约0.3秒)有效说话人特征，但d-vector系统的Top-1 IDR(\%)已超过了50\%。
与之相比，i-vector系统在此条件下的Top-1 IDR(\%)仅有6\%。
这也再次验证了所学说话人特征具有极强的说话人区分性。
此外，如表~\ref{tab:result-sid-long} 所示，该说话人辨认任务与上节说话人确认任务有着一致的规律。
随着测试语音时长的增多，i-vector和d-vector的系统性能均有所提升，
但i-vector系统的提升程度更为显著。
对d-vector系统而言，与说话人确认任务不同的是，在说话人辨认中，PLDA打分度量方法均优于Cosine和LDA方法。
我们认为这可能是因为在说话人辨认任务中，说话人之间的相对分数比全局分数更为重要，
而这种相对分数受PLDA模型的高斯假设影响相对较小，使其在辨认任务中取得了更好的性能表现。
当然，我们仍有待更深入地研究PLDA模型对d-vector的影响。

\begin{table}[htb]
	\begin{center}
		\caption{短时测试场景下的说话人辨认识别结果}
		\label{tab:result-sid-short}
		\resizebox{0.75\linewidth}{!}{
			\begin{tabular}{|c|l|p{2cm}<{\centering}|p{2cm}<{\centering}|p{2cm}<{\centering}|}
				\hline
				测试系统   &  打分度量 & \multicolumn{3}{c|}{短时场景Top-1 IDR(\%)} \\\cline{3-5}
				%\hline
				&       & S(30-21f) & S(30-51f) & S(30-101f)  \\
				\hline
				i-vector   &  Cosine  &   5.16    &  20.86   &  42.25   \\
				&  LDA     &   5.97    &  27.32   &  54.52    \\
				&  PLDA    &   6.31    &  30.07   &  58.40    \\
				\hline
				d-vector   &  Cosine  &   50.60   &  66.19   &  79.18   \\
				&  LDA     &   46.67   &  65.71   &  80.89    \\
				& PLDA     & \textbf{51.52} & \textbf{69.63} & \textbf{83.71}  \\
				\hline
			\end{tabular}
		}
	\end{center}
\end{table}

\vspace{-2mm}

\begin{table}[htb]
	\begin{center}
		\caption{长时测试场景下的说话人辨认识别结果}
		\label{tab:result-sid-long}
		\resizebox{0.75\linewidth}{!}{
			\begin{tabular}{|c|l|p{2cm}<{\centering}|p{2cm}<{\centering}|p{2cm}<{\centering}|}
				\hline
				测试系统   &  打分度量  & \multicolumn{3}{c|}{长时场景Top-1 IDR(\%)} \\\cline{3-5}
				%\hline
				&        &  L(30-3) & L(30-9) & L(30-18)  \\
				\hline
				i-vector   &  Cosine   &   82.26  &  96.43  &  98.39  \\
				&  LDA     &   88.92  &  97.58  &  98.71   \\
				&  PLDA    &   89.61  &  \textbf{97.84} & \textbf{98.74} \\
				\hline
				d-vector   &  Cosine   &   87.99  &  92.18  &  93.26  \\
				&  LDA     &   89.91  &  93.68  &  94.51   \\
				& PLDA     & \textbf{91.90} & 95.41 &  96.10   \\
				\hline
			\end{tabular}
		}
	\end{center}
\end{table}

\vspace{-4mm}

\subsection{模型分析}

\subsubsection{$i$-$vector$ 模型与 $d$-$vector$ 模型的比较}

通过定性和定量的分析，我们验证了所学说话人特征的有效性。
本节我们将从三个方面对比分析当前主流的基于概率统计的i-vector模型
和本文所提出的基于特征学习的d-vector模型之间的差异。

\begin{itemize}
	
	\item \textbf{模型性质}:
	i-vector是一个“产生式”模型，其通过构建一个线性高斯模型来实现对说话人的建模；
	d-vector是一个“区分性”模型，其通过层次性网络结构来实现说话人特征的学习。
	\item \textbf{训练准则}:
	i-vector基于最大似然准则，通过无监督的学习方法描述整个声学空间；
	d-vector基于最大区分性准则，通过有监督的学习方法最大化的区分不同说话人。
	\item \textbf{描述能力}:
	i-vector描述了一个全变量子空间，该子空间中蕴含了包括说话人因子在内的各种信息因子；
	d-vector描述了一个说话人子空间，该子空间仅用于描述说话人相关的特性。

\end{itemize}

\subsubsection{CT-DNN 模型设计中的若干经验}

对于本章提出的CT-DNN模型，我们在实验中尝试了各种网络结构与配置，并发现了若干经验，总结如下：

\begin{itemize}
	
	\item 相比于卷积层，时延层对说话人特征学习更为重要。
	尽管加入卷积层对说话人特征学习有着很好的作用，但由于卷积层的网络参数量较大(滤波器个数决定)，
	使网络训练和特征提取的效率偏低。
	\item 与其它激活函数(如Sigmoid、ReLU)相比，本章所采用的基于$p$-范数的非线性激活函数更为有效。
	将$p$-范数用于隐藏层结点的组归一化，一来减少了网络参数量，提高了训练效率；
	二来实现了特征的非线性降维，避免了所学说话人特征的稀疏性。
		
\end{itemize}

\section{小结}

本章从语音信号的基本特性出发，结合说话人信息在语音信号中的表征形式，
针对语音信号的局部属性、动态属性和模型的可训练性，设计了一个包含卷积、时延和组归一化
的卷积-时延深度神经网络(CT-DNN)模型，用于说话人特征学习。
通过定性和定量分析，本章验证了所学说话人特征具有很强的说话人区分性。
最后，本章从多个角度对比分析了基于概率统计的i-vector模型与基于特征学习的d-vector模型之间的差异。
