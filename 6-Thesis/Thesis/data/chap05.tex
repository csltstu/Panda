\chapter{基于音素相关训练的说话人特征学习}
\label{cha:phone-aware}

\section{本章引论}

在第~\ref{cha:fit-ctdnn} 章，我们从CT-DNN模型自身出发，
提出了基于类中心趋近准则的全信息训练模型FIT CT-DNN，
解决了说话人特征在提取过程中“信息泄露”的缺陷，提升了说话人特征的类内内聚性。
然而，无论是基础的CT-DNN模型还是改进的FIT CT-DNN模型，
其在特征学习过程中都是完全依赖于复杂的模型结构和大量的语音数据，
而并没有引入任何先验知识。
通过说话人区分性训练，神经网络逐渐从原始特征中移除与说话人无关的信息，而保留与说话人相关的信息。

然而，这种“盲目”的数据驱动方式使得网络在训练过程中极易受到各种干扰因素的影响，
导致模型训练的不稳定性。
在第~\ref{cha:ct-dnn} 章、第~\ref{cha:analysis} 章和第~\ref{cha:fit-ctdnn} 章中，
我们通过可视化分析观察到，基于CT-DNN模型所学到的说话人特征在一个连续语音片段中
的分布呈现出一个与文本内容相关的轨迹。
这表明在说话人特征中仍隐藏着某些发音内容信息，
而这些发音内容信息导致了说话人特征的类内发散性，影响了说话人识别系统的性能。

为了削弱发音内容信息对说话人特征的扰动，
本章首先分析了语音信号中发音内容信息和说话人信息之间的互斥关系。
在此基础上，为了在说话人特征学习中更好地利用这种互斥关系来削弱发音内容信息的干扰，
我们受条件学习的启发，尝试将发音内容信息作为一种条件知识，辅助说话人特征的学习。
为此，本章提出了一个基于音素相关训练的PAT CT-DNN模型。
该模型的基本思想是在基础CT-DNN模型中先验地引入音素信息，
使说话人特征在学习过程中得到音素先验的补偿，
以此解决因发音内容不同而导致的说话人特征发散的问题。
实验表明，与 基础CT-DNN 模型相比，该 PAT CT-DNN模型在不同测试条件下取得了一致的性能提升。
此外，在基于条件学习的PAT CT-DNN模型的基础上，我们又开展了相关扩展性研究，
提出了协同联合训练方法和级联深度分解模型，用于语音信号中的多任务协同学习和信号深度分解。

\section{问题分析}

在前三章中，
我们分别从定性和定量两个角度对所学说话人特征的属性有了深入的理解。
通过定性的可视化分析，我们发现每个说话人的特征空间具有较强的内聚性。
这表明在特征学习的过程中，神经网络能够从原始声学特征中保留与说话人相关的信息，
而移除与说话人无关的干扰信息，如发音内容信息。
然而，我们同样注意到所学说话人特征仍会随着发音内容的改变而发生改变。
从图~\ref{fig:phone-blind-3} (b)
\footnote{为了论文表达清晰，此处复制了第~\ref{cha:ct-dnn} 章中的t-SNE可视化图~\ref{fig:phone-blind}。}
中不难发现，所学说话人特征在某个连续语音片段中具有文本相关的模式，
其分布呈现出一个与文本内容相关的轨迹。
此外，通过定量的泛化分析(第~\ref{cha:analysis} 章)，我们发现所学到的说话人特征
在不同文本条件下的高斯性大有不同。在文本相关的条件下，该特征具有较为明显的高斯性；
而在文本无关的条件下，该特征呈现出极强的非高斯性。
这些现象表明所学说话人特征受发音内容的影响而表现出不同的分布特性。

综上所述，在所学到的说话人特征中仍掺杂着部分与发音内容相关的信息，
而这些发音内容信息导致每个说话人的特征空间存在着类内发散的问题，
使之影响了说话人识别系统的性能。
因此，本章的研究目标是如何进一步滤除或解释所学说话人特征中的发音内容信息，提取更具有区分性的说话人特征。

\begin{figure}[H]
	\centering
	\includegraphics[width=0.90\linewidth]{phone-blind}
	\caption{基于t-SNE的说话人特征可视化}
	\label{fig:phone-blind-3}
\end{figure}

\section{音素相关训练}

为了进一步滤除说话人特征中的发音内容信息，本节首先从语音信号处理的角度出发，
分析了语音信号中发音内容信息和说话人信息之间的关系；
并在此基础上，受条件学习的启发，设计了一个基于音素补偿准则的音素相关训练方法。
通过在说话人特征学习过程中先验地引入音素条件，
使所学特征即时得到音素信息的补偿，从而更好地削弱了发音内容信息对说话人特征的干扰。

\subsection{条件学习}
\label{cha:condition}

语音信号中蕴含着丰富的信息。
针对不同信息，人们提出了不同的识别任务。
对语音识别而言，其目标是从语音信号中提取出不同说话人所共享的发音模式。
显然，语音信号中的说话人信息是语音识别任务的主要干扰。
相反，对说话人识别而言，其目标是从语音信号中提取出与发音内容无关的说话人个性信息。
显然，语音信号中的发音内容信息是说话人识别任务的主要干扰。
因此，语音识别和说话人识别可被视为一对\textbf{互斥任务}，即两个任务所需要的信息是完全不同的，
并且一个任务所需的信息恰好是另一个任务的干扰。
由于二者之间所需信息是互斥的，因此在任务学习过程中，两个任务难以实现信息共享。

然而，从另一个角度看，如果我们知道了某一个任务的信息，则对另一个任务显然也是有帮助的。
例如，在语音识别中，特定说话人的语音识别总是比非特定说话人的语音识别性能好；
类似地，在说话人识别中，文本相关任务比文本无关任务更加容易。
这说明即使是互斥任务，对方任务的信息也是非常重要的。
通过了解对方信息，可以在任务学习时以对方信息为\textbf{辅助条件}，来指导自身任务的学习，
从而大幅降低了学习的复杂度。我们称这一学习方法为\textbf{条件学习}，
如图~\ref{fig:condition} 所示。
以语音识别来辅助说话人识别为例。
语音信号中的原始特征(如Fbanks)为图中的输入$x$；
说话人识别为目标任务$t$；语音识别为辅助任务$c$。
对于单一任务的说话人识别$t$而言，其目标是给定输入特征$x$，预测出目标$t$的后验概率$P(t|x)$。
若将语音识别$c$作为辅助条件，则对后验概率$P(t|x)$的计算将变成了一个边缘概率的计算问题，
其可表示为$\sum_{c} P(t|x,c)P(c|x)$。
其中，$P(c|x)$为给定输入特征$x$，预测出目标$c$的后验概率，
其代表了每个输入特征$x$在不同音素上的概率分布，
而该分布作为先验条件用于$P(t|x)$的计算中。
显然，基于条件学习的方法将后验概率的计算问题转变成边缘概率的计算问题，
这种学习方法具有两个重要的优势：

\begin{itemize}
	\item $P(c|x)$作为条件学习中的辅助条件，其可以从一个与目标任务$t$完全无关的辅助任务$c$中学习到。
	因此，条件学习具有灵活、高效的优势。
	\item 相比于单一任务学习，条件学习通过引入先验知识$P(c|x)$，
	降低了模型在训练过程所受隐藏变量$c$的影响，使模型训练更加简单、容易。
\end{itemize}

\begin{figure}[H]
	\centering
	\includegraphics[width=1\linewidth]{condition}
	\caption{条件学习示意图}
	\label{fig:condition}
\end{figure}

基于条件学习的上述优势，研究者们已开展了一系列相关研究。
例如，Saon和Karanasou等人~\cite{saon2013speaker,karanasou2014adaptation}发现
在语音识别模型中加入说话人信息(如i-vector)，使语音识别性能得到了显著提升；
同样，Kenny和Lei等人~\cite{kenny2014deep,lei2014novel}利用语音识别系统所输出的音素后验概率
作为先验知识，建立说话人识别模型，有效地提高了说话人识别的性能。

\subsection{模型设计}

针对本章的研究目标，为了削弱发音内容信息对所学说话人特征的扰动，
我们受条件学习的启发，提出了一个基于音素相关训练(Phone-aware training, PAT)的CT-DNN模型。
该模型的基本思想是在CT-DNN模型中先验地引入音素条件，使说话人特征在学习过程得到了音素先验知识的指导，
以此解决因发音内容不同而导致的说话人特征发散。
图~\ref{fig:ct-dnn-phone} 给出了基于PAT CT-DNN的说话人特征学习模型。

\begin{figure}[H]
	\centering
	\includegraphics[width=1\linewidth]{ct-dnn-phone}
	\caption{基于PAT CT-DNN的说话人特征学习模型}
	\label{fig:ct-dnn-phone}
\end{figure}

与基础CT-DNN模型相比，该PAT CT-DNN 模型在网络瓶颈层中引入了与发音内容相关的音素信息。
为了得到音素信息，我们首先基于Kaldi~\cite{povey2011kaldi} THCHS-30标准中文语音识别流程，
在一个$1,400$小时的中文电话信道语料库上训练了一个基于深度神经网络的语音识别系统(DNN-ASR)。
该DNN-ASR模型采用$40$维的Fbanks特征，拼接前后各$1$帧的上下文信息，构成了$120$维的特征向量作为DNN的输入。
此外，DNN-ASR模型共由$7$个时延隐藏层组成，每层包含$1,024$个结点；
输出层共有$3,509$个结点，等同于HMM状态经过GMM聚类后的三音素状态(senones)的个数。

在DNN-ASR模型训练完成后，倘若我们选取各个音素状态的后验概率作为音素特征，则会存在两个弊端。
一来该音素特征的维度过大(共$3,509$维)，难以引入到CT-DNN模型中；
二来该音素特征具有较强的稀疏性，大多数音素状态的后验概率通常趋近于0。
因此，为了得到更低维的紧凑的音素信息表征，我们首先采用奇异值分解(SVD)算法
将已训练完成的DNN-ASR模型的最后一层仿射变换矩阵进行分解；分解得到两个低秩矩阵，其秩为$40$。
而后在此基础上，再对整个网络进行优化，直至模型收敛。

在PAT CT-DNN模型训练时，首先将$40$维的音素特征从DNN-ASR的低秩矩阵中读取出来，并加入到基础CT-DNN模型中；
之后的网络训练过程与基础CT-DNN模型完全一致。
将低维音素特征加入到基础CT-DNN模型的方式有很多种，例如音素特征与卷积层的输入特征拼接、
音素特征与时延层中的隐藏层拼接、音素特征与瓶颈层拼接等等。
为此，我们首先分析这几种特征拼接方式的合理性，并选出一个相对最优的方式。

\begin{itemize}
	\item \textbf{音素特征与卷积层的输入特征拼接}:
	音素特征是DNN-ASR模型从大量数据中学习到的与音素相关的特征，其仅代表了语音信号中的音素信息；
	而PAT CT-DNN模型的输入特征(Fbanks)中则蕴含着各种信息。
	显然，这两种特征有着不同的特征属性，代表了不同的特征模式。
	对卷积层而言，其目标是学习特征中的局部共享模式。
	因此，将这两种特征拼接后进行卷积处理是不合理的。
	\item \textbf{音素特征与时延层中的隐藏层拼接}：
	首先，时延层由$4$个隐藏层组成，每个隐藏层有$2,000$个结点。
	若将$40$维的音素特征与隐藏层中的$2,000$个结点相拼接，
	由于音素特征维度过低，使得网络在训练过程中对音素信息不够敏感。
	若增大音素特征维度，则将会增加网络参数量和训练复杂度。
	其次，若将$40$维的音素特征与组归一化后的$400$维输出相拼接，
	由于网络时延的存在，使得PAT CT-DNN模型在训练过程中还需要考虑音素特征的时延。
	显然，这样将会增大网络的训练难度，降低网络的训练效率。
	\item \textbf{音素特征与瓶颈层拼接}：
	瓶颈层位于卷积层和时延层的中间，其起到了一个承上启下的作用。
	原始声学特征经过卷积层的滤波，逐渐学习到数据中的共享模式，而滤除了与之无关的干扰(如信道、噪音等)，
	使得瓶颈层中的特征具有普适性和通用性。
	此外，受网络目标函数的制约，瓶颈层中的特征又具有一定的任务相关性。
	因此，若在瓶颈层中引入音素特征，不仅能够使整个网络在训练过程中充分地利用音素信息，
	而且其在本质上并没有改变网络结构，使网络保持了原有的训练方式。
	
\end{itemize}

综上所述，我们最终采用音素特征与瓶颈层拼接的方式，构建了完整的PAT CT-DNN模型，如图~\ref{fig:ct-dnn-phone} 所示。

\subsection{讨论分析}

与基础CT-DNN模型和FIT CT-DNN模型相比，本章提出的PAT CT-DNN模型具有以下特点：

\begin{itemize}
	\item
	PAT CT-DNN模型的设计灵感来源于条件学习。在训练过程中，通过先验地引入音素信息来指导说话人特征的学习，
	使学习到的说话人特征具有更强的音素无关性，解决因发音内容不同而导致的说话人特征发散问题。
	\item
	PAT CT-DNN模型在本质上并没有改变网络结构，其训练流程与CT-DNN模型一致。
	因此，与FIT CT-DNN模型相比，PAT CT-DNN模型的训练过程更简单清晰。
	\item
	PAT CT-DNN模型在训练过程中需要额外地引入音素信息，其需预先训练得到一个DNN-ASR模型。
	因此，与CT-DNN、FIT CT-DNN模型相比，PAT CT-DNN模型增加了额外的开销。

\end{itemize}

\section{实验}
\label{cha:exp-pat}

为了验证PAT CT-DNN模型的有效性，我们设计了相关对比实验。
本节将首先介绍所用的实验数据和系统配置；
而后给出相关实验结果与分析。

\subsection{实验数据}

\begin{itemize}
	
	\item \textbf{PAT CT-DNN模型训练集}: 
	我们继续选用从英文电话信道 \emph{Fisher} 数据库中随机挑选的$5,000$个说话人所组成的训练集，
	用于PAT CT-DNN模型的训练。
	具体数据组成与~\ref{cha:data} 节一致。
	
	\item \textbf{DNN-ASR模型训练集}: 
	我们选用了一个$1,400$小时的中文电话信道数据库用于DNN-ASR模型的训练，
	其语音数据的采样率为8kHz，采样精度为16bits。
	
	\item \textbf{Fisher 1000测试集}: 
	我们继续选用从英文电话信道 \emph{Fisher} 数据库中随机挑选的$1,000$个说话人所组成的测试集，
	并复用了短时和长时两种测试场景。
	具体数据组成和测试配置与~\ref{cha:verification} 节一致。
	
	\item \textbf{CSLT-CUDGT2014测试集}: 
	为了进一步验证PAT CT-DNN模型所学特征对发音内容的鲁棒性，我们将其应用到
	跨语言说话人识别任务中。我们选用 \emph{CSLT-CUDGT2014} 汉-维双语数据库作为测试集。
	具体数据组成和测试配置与~\ref{cha:exp-cross} 节一致。

\end{itemize}

\subsection{系统配置}

本节首先建立了 i-vector 和 d-vector 两个说话人识别系统作为基线系统。
两个基线系统的模型配置与前文~\ref{cha:quantity} 节一致，此处将不再赘述。
我们将基于PAT CT-DNN模型所构建的说话人识别系统简称为 ‘d-vector + PAT’ 系统。
在PAT CT-DNN模型训练和说话人特征提取时，首先将原始声学特征(Fbanks)通过DNN-ASR系统，
获取每帧语音所对应的音素特征；然后将其拼接在瓶颈层之后；后续过程与基础CT-DNN模型一致。
同样地，本实验使用了三种打分策略：(1) 基于原始$400$维向量的余弦距离；
(2) 基于LDA变换后$150$维向量的余弦距离；
(3) 原始$400$维向量经过中心化和长度归一化后的PLDA打分。
此外，选用等错误率(EER)作为系统性能的评价指标。

\subsection{实验结果}

\subsubsection{不同时长场景下的测试}

\begin{table}[htb]
	\begin{center}
		\caption{短时测试场景下的说话人确认识别结果}
		\label{tab:result-sv-short-phone}
		\resizebox{0.75\linewidth}{!}{
			\begin{tabular}{|c|l|p{2cm}<{\centering}|p{2cm}<{\centering}|p{2cm}<{\centering}|}
				\hline
				测试系统       &  打分度量 & \multicolumn{3}{c|}{短时场景EER(\%)}  \\\cline{3-5}
				%\hline
							  &            & S(30-21f) & S(30-51f) &  S(30-101f)   \\
				\hline
				i-vector      &    Cosine   &  30.01    &  18.23    &  11.14       \\
							  &    LDA      &  29.47    &  15.96    &   8.64       \\
							  &    PLDA     &  29.29    &  15.71    &   8.34       \\
				\hline
				d-vector      &  Cosine     &   8.31    &  7.09     &   4.77      \\
							  &    LDA      &   8.48    &  4.92     &   3.02       \\
							  &    PLDA     &  24.63    &  17.47    &  10.45        \\
				\hline
				d-vector      &  Cosine     & \textbf{7.00}  &  5.98     &   4.34      \\
				+ PAT 		  &    LDA      &   7.55    & \textbf{4.55} & \textbf{2.92}   \\
							  &    PLDA     &  21.41    &  15.10    &   8.92        \\
				\hline
			\end{tabular}
		}
	\end{center}
\end{table}

\begin{table}[htb]
	\begin{center}
		\caption{长时测试场景下的说话人确认识别结果}
		\label{tab:result-sv-long-phone}
		\resizebox{0.75\linewidth}{!}{
			\begin{tabular}{|c|l|p{2cm}<{\centering}|p{2cm}<{\centering}|p{2cm}<{\centering}|}
				\hline
				测试系统       &  打分度量 & \multicolumn{3}{c|}{长时场景EER(\%)} \\\cline{3-5}
				%\hline
							  &             &  L(30-3) & L(30-9) & L(30-18)  \\
				\hline
				i-vector      &    Cosine   &   3.77   &  1.09   &\textbf{0.53}   \\
				   			  &    LDA      &   3.11   &  1.01   &    0.63       \\
							  &    PLDA     &   3.04   &\textbf{0.88} &  0.57   \\
				\hline
				d-vector      &  Cosine     &   3.79   &  2.56   &  2.30         \\
							  &    LDA      &   2.13   &  1.48   &  1.33         \\
							  &    PLDA     &   7.96   &  4.06   &  3.59         \\
				\hline
				d-vector      &  Cosine     &   3.75   &  2.44    &  2.21      \\
				+ PAT 		  &    LDA   & \textbf{2.12} &  1.42    &  1.30       \\
							  &    PLDA     &   7.09   &  3.58    &  3.14        \\
				\hline
			\end{tabular}
		}
	\end{center}
\end{table}

在短时和长时测试场景下的实验结果分别如表~\ref{tab:result-sv-short-phone} 
和 表~\ref{tab:result-sv-long-phone} 所示。
首先通过对比 i-vector 基线系统和 d-vector + PAT系统，
我们可以看出d-vector + PAT 系统继承了d-vector基线系统在短时场景下的优势，
其在短时场景下取得了比i-vector基线系统更好的性能；
而随着测试语音时长的增多，i-vector基线系统逐渐超越了d-vector + PAT系统。

其次通过对比 d-vector 基线系统和 d-vector + PAT 系统，
我们发现 d-vector + PAT系统性能在全部测试场景/条件下均超越了d-vector 基线系统。
更值得注意的是，在短时场景下，基于原始d-vector的Cosine打分策略，
d-vector + PAT系统性能与d-vector基线系统之间有着显著的差距
(例如，在S(30-21f)下，等错误率EER从8.31\%降至7.00\%)。
这表明PAT CT-DNN模型在训练过程中，通过先验地引入音素信息，有效地削弱了发音内容对说话人特征的扰动，
降低了说话人的类内离散度，提升了说话人识别系统的性能。

\subsubsection{跨语言场景下的测试}

为了进一步验证基于PAT CT-DNN模型所学说话人特征对发音内容的鲁棒性，
我们将所学说话人特征应用到跨语言说话人识别中，其实验结果如表~\ref{tab:exp-cross-2} 所示。

\begin{table}[htb]
	\begin{center}
		\caption{跨语言说话人识别结果}
		\label{tab:exp-cross-2}
		\resizebox{0.8\linewidth}{!}{
			\begin{tabular}{|p{2cm}<{\raggedright}|p{2cm}<{\raggedright}|p{2cm}<{\centering}|p{2cm}<{\centering}|p{2cm}<{\centering}|}
				\hline
				\multicolumn{2}{|c|}{}     &\multicolumn{3}{c|}{测试场景EER(\%)}\\
				\hline
				测试系统       &  打分度量   & 汉语-汉语 & 维语-维语 & 汉语/维语  \\
				\hline
				\hline
				i-vector      &    Cosine   &   7.55   &  6.16   &  15.14     \\
				  			  &    LDA      &   6.30   &  5.63   &  12.77     \\
							  &    PLDA     &   5.31   &  4.29   &  9.82      \\
				\hline
				d-vector      &    Cosine   &   4.71   &  4.09   &  10.45     \\
							  &    LDA      &   6.64   &  5.47   &  13.16     \\
							  &    PLDA     &   3.75   &  3.71   &  8.66  \\
				\hline
				d-vector      &    Cosine   &   4.07   &  4.03   &  10.30     \\
				+ PAT 		  &    LDA      &   6.09   &  5.21   &  13.02     \\
							  &    PLDA   &\textbf{3.61} & \textbf{3.52} & \textbf{8.37} \\
				\hline
			\end{tabular}
		}
	\end{center}
\end{table}

首先通过对比 i-vector 基线系统和 d-vector + PAT系统，
我们发现其实验现象与~\ref{cha:exp-cross} 节是一致的。
由于该测试集的文本内容仅局限于数字文本，因此得到的d-vector在该任务中具有更强的高斯性，
使得PLDA打分度量在d-vector + PAT系统中取得了最优的性能表现。

此外，与d-vector基线系统相比，d-vector + PAT系统在不同测试条件和度量方式下的识别性能均有所提高。
这表明在模型训练中加入音素信息有利于减轻网络对音素扰动的学习负担，
进一步削弱了说话人特征中的发音内容信息，使得到的特征更具有说话人区分性。

\subsection{实验分析}

为了进一步分析PAT CT-DNN模型的优势，本节我们分别从训练过程和可视化两个角度开展了相关工作。

\subsubsection{训练过程}

图~\ref{fig:pat-acc} 给出了在整个训练过程中，基础CT-DNN模型和PAT CT-DNN模型分别在训练集和验证集上帧准确率
的变化情况。
从图中可以看出，PAT CT-DNN模型在训练集和验证集上的帧准确率均高于基础CT-DNN模型在训练集和验证集上的帧准确率。
从模型最终收敛的情况来看，基础CT-DNN模型在训练集上的帧准确率为$92.55\%$，在验证集上的帧准确率为$58.45\%$；
而PAT CT-DNN模型在训练集上的帧准确率为$95.63\%$，在验证集上的帧准确率为$64.20\%$。
PAT CT-DNN模型的帧准确率在训练集上的提高，表明在训练中引入音素信息，有利于提升模型对目标任务的优化能力；
而帧准确率在验证集上的提高，表明通过引入音素信息，有效地提升了所学说话人特征的泛化能力。

\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\linewidth]{pat-acc-2}
	\caption{基础CT-DNN模型和PAT CT-DNN模型在训练集和验证集上的帧准确率变化情况}
	\label{fig:pat-acc}
\end{figure}

\subsubsection{t-SNE可视化}

为了更好地观察对比基础CT-DNN模型所学特征与PAT CT-DNN模型所学特征之间的差异，
我们从测试集中随机挑选了$20$个说话人，并从每个说话人的语音中截取了一个连续的语音片段。
与前文类似，我们采用t-SNE方法将$400$维的说话人特征映射到一个二维空间中，实现说话人特征的可视化，
如图~\ref{fig:compare} 所示。

从图中可以看出，两个模型均具有很强的说话人区分性。
更具体地，与图~\ref{fig:compare} (a)相比，图~\ref{fig:compare} (b)中
每个说话人特征的分布轨迹显得更为“紧凑”。
尤其是椭圆形框所标注的位置，同一个说话人的语音片段，其在图~\ref{fig:compare} (a)呈现长条状
或者被分成了两个子空间(图中红色虚线框)；
而对应在图~\ref{fig:compare} (b)中则显得更为内聚(图中绿色实线框)。
这表明，与基础CT-DNN模型相比，PAT CT-DNN模型通过在训练中引入音素信息，
削弱了发音内容信息对说话人特征的影响，提升了说话人特征的类内内聚性。

\begin{figure}[H]
	\centering
	\includegraphics[width=0.90\linewidth]{compare}
	\caption{基于t-SNE的说话人特征可视化。其中，(a)代表基础CT-DNN模型所提取的说话人特征；
		(b)代表PAT CT-DNN模型所提取的说话人特征。每种颜色代表一个说话人。}
	\label{fig:compare}
\end{figure}

\section{扩展性研究}

当前对语音信号处理的研究在很大程度上都是割裂的：语音识别的研究者通常只关注于如何从语音信号中得到发音内容，
而将说话人等信息视为噪音，采用各种方法将其滤除；
说话人识别的研究者则将发音内容等信息视为主要干扰因素，设计各种正规化方法以去除其影响。
其它领域的研究（如情感识别、语种识别等）也有同样倾向，即只关注本领域需要的信息，而将其它信息作为干扰和噪音。

基于条件学习的PAT CT-DNN模型的成功探索，使我们意识到语音信号中各种信息之间并非是相互敌对、彼此独立的，
而是相互依赖、彼此促进的。
在某一任务的训练过程中先验地引入其它任务信息作为学习条件，将有利于提高该任务的训练效率和识别性能。
显然，这种信息交互的学习模式更符合人类对语音信号的认知。
人类在听到一个声音的时候，其并不是对某一种信息的单任务串行处理，而是对所有信息的多任务并行处理。
更重要的是，通过信息之间的交互传递，使各个任务之间相互协同、共同促进。
为此，本节我们将这种信息交互的学习模式扩展至语音信号处理的其它领域中，
实现了语音信号的多任务学习与深度分解。

\subsection{协同学习}
\label{cha:joint}

在本章~\ref{cha:condition} 节，我们提到条件学习方法以其简单灵活的信息传递方式，
在语音识别和说话人识别等领域得到了广泛的关注。
受此启发，我们提出了一种基于音素相关训练的PAT CT-DNN模型，通过在模型训练中先验地引入音素信息实现对基础CT-DNN模型的优化。
实验表明，与基础CT-DNN相比，PAT CT-DNN模型在训练集和验证集上的帧准确率均有所提升，
其进一步削弱了发音内容对所学说话人特征的扰动，使所学特征具有更好的性能表现。

然而，尽管条件学习方法取得了一定的成功，但仍存在一定的局限性。
首先，在训练过程中，每个任务的训练在本质上仍是相互独立的。
如图~\ref{fig:condition} 所示，在条件学习中，辅助任务首先完成训练，然后再提供信息给目标任务。
因此，该条件学习并没有实现训练的联合优化。
其次，在识别过程中，整个信息传递是单向的，仅从辅助任务流向目标任务，
两个任务之间没有任何反馈。因此，该条件学习并没有实现两个任务的联合优化。
		
显然，这种单向条件学习的策略与人类学习识别的过程仍有差距。
人类在学习(训练)和识别的过程中，对多任务是并行处理的，
并即时从其它任务中得到反馈，从而实现了多任务学习和识别的联合优化。
我们称这种并行处理、即时反馈、联合优化的学习和识别方法为“\textbf{协同学习}”。

\begin{figure}[H]
	\centering
	\includegraphics[width=0.55\linewidth]{joint}
	\caption{协同学习示意图}
	\label{fig:joint}
\end{figure}

图~\ref{fig:joint} 给出了协同学习的示意图。
其中，$x(t)$是两个目标任务在$t$时刻的共享输入特征(例如，声学特征Fbanks)；
$Q(t)$和$L(t)$分别对应两个目标任务在$t$时刻的预测输出，而它们将作为即时信息在\textbf{下一时刻}传递给对方，实现相互反馈、共同优化的多任务协同训练。

\subsubsection{模型设计}

为了验证协同学习的有效性，本节以语音识别和说话人识别为例，开展了一系列相关探究。
考虑到语音信号的时序性，为了更好地实现协同学习中的信息即时反馈，我们选用了对时序信号有着更好描述能力的循环神经网络，
并以长短时记忆循环神经网络(LSTM)~\cite{graves2014towards}为例，
设计了一个基于协同联合训练(Collaborative joint training, CJT)的循环神经网络模型。
该模型是我们在~\inlinecite{sak2014long}所提出的LSTM模型基础上，
针对协同学习任务而设计得到的，其如图~\ref{fig:mutli-recurrent} 所示。

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\linewidth]{mutli-recurrent}
	\caption{基于协同联合训练的循环神经网络(CJT LSTM), 以语音识别和说话人识别为例}
	\label{fig:mutli-recurrent}
\end{figure}

该模型是由两个LSTM模型组成，其训练目标分别对应于语音识别$y_{t}^{a}$和说话人识别$y_{t}^{s}$任务。
为了实现信息的即时反馈，我们将其中一个LSTM模型在$t$时刻的输出(如非循环映射层$r_{t}$、循环映射层$p_{t}$)
传递给另一个LSTM的输入(如输入控制门$i$、遗忘控制门$f$、输出控制门$o$、非线性激活函数$g(\cdot)$)，
作为下一时刻模型训练的条件。
可见，两个LSTM模型之间信息传递的组合方案有很多种。
图~\ref{fig:mutli-recurrent} 给出了一种具体方案，如图中红线和绿线所示。
其中，一个LSTM模型的非循环映射层$r_{t}$和循环映射层$p_{t}$在$t$时刻的输出(反馈信息)
作为下一时刻另一个LSTM模型的非线性激活函数$g(\cdot)$的输入。
通过这种信息传递，实现了两个任务的协同训练。
值得注意的是，在我们开展相关工作的同时，Li等人~\cite{li2015modeling}也提出了类似的模型结构。
与之不同的是，Li等人的工作更关注于将说话人信息作为一种辅助手段来
提升语音识别的性能，而我们的工作则更关注于语音识别和说话人识别通过协同学习的方式来相互促进两个任务的性能。

\subsubsection{实验分析}

与单一任务学习相比，多任务学习需要训练数据中同时具备各个任务的标注信息。
为此，我们选择了同时具备语音内容标注和说话人标注的英文\emph{WSJ} 数据库。
为了与~\emph{Fisher} 保持同样的特征配置，在本实验中将\emph{WSJ} 中16kHz的语音数据降采样到8kHz。
训练集 train\_si284 中共有$282$个说话人、$37,318$条语音，其中每个说话人约有 $50$-$155$ 条语音。
测试集由三个数据集(devl92, eval92 和 eval93)组成，共有$27$个说话人、$1,049$条语音。
对于说话人识别，该测试集共计有目标测试$21,350$次、闯入测试$528,326$次。
本实验是基于Kaldi~\cite{povey2011kaldi} WSJ s5 nnet3的流程完成的。

为了验证协同联合训练CJT LSTM模型的有效性，我们设计了相关对比实验。

\begin{itemize}
	
	\item \textbf{基线系统}：语音识别(ASR)基线系统使用了单层LSTM，隐藏层有$1,024$个细胞，
	非循环映射层$r_{t}$和循环映射层$p_{t}$的输出均为$256$维。
	模型输入为$40$维Fbanks特征，考虑上下文各$2$帧，共计$200$维；输出共有$3,377$个结点(等同于senones个数)。
	ASR基线系统的词错误率(WER)为10.30\%。
	说话人识别(SRE)基线系统共有两个，一个是i-vector系统，一个是基于LSTM的r-vector系统；
	i-vector系统配置与~\ref{cha:config} 节基本一致，不同的是i-vector维度设为$200$。
	r-vector系统的网络结构与基于LSTM的ASR基线系统相同，
	唯一不同的是非循环映射层$r_{t}$和循环映射层$p_{t}$的输出设为$128$维；
	输出结点为对应训练集中的$282$个说话人。
	类似于前文d-vector的提取方式，
	将每一帧$r_{t}$和$p_{t}$的输出合并平均，得到说话人向量‘r-vector’，
	其维度为$256$。与前文结果一致，i-vector采用PLDA打分度量效果最佳，取得了EER为1.06\%的性能；
	r-vector采用LDA打分度量效果最佳，EER为1.77\%。
	
	\item \textbf{条件学习系统}：
	类似于PAT CT-DNN模型的学习流程，将ASR基线系统的$r_{t}^{a}$和$p_{t}^{a}$输出(音素特征)作为
	条件辅助SRE LSTM模型的训练，得到基于音素相关训练(Phone-aware training)的r-vector系统，
	简称为PAT SRE系统。
	同样地，将SRE基线系统的$r_{t}^{s}$和$p_{t}^{s}$输出(说话人特征)作为
	条件辅助ASR LSTM模型的训练，得到说话人相关训练(Speaker-aware training, SAT)的ASR系统，
	简称为SAT ASR系统。
	
	\item \textbf{协同学习系统}：
	采用图~\ref{fig:mutli-recurrent} 中对称的协同学习结构，将每个任务$r_{t}$和$p_{t}$的输出作为另一个任务在	下一时刻$g(\cdot)$函数的输入条件，得到协同联合训练CJT LSTM系统。
	
\end{itemize}

\begin{table}[htb]
	\begin{center}
		\caption{基线系统、条件学习系统和协同学习系统的性能对比}
		\label{tab:collr}
		\resizebox{0.7\linewidth}{!}{
			\begin{tabular}{|p{3.5cm}<{\raggedright}|p{3cm}<{\centering}|p{3cm}<{\centering}|}
				\hline
				测试系统                  &  ASR (WER\%) & SRE (EER\%)     \\ 
				\hline 
				\hline
				ASR基线                   &  10.30      & -          \\
				SAT ASR                   &  9.97      & -          \\
				\hline
				SRE基线(r-vector)          & -          & 1.77          \\
				SRE基线(i-vector)         & -          & 1.06      \\
				PAT SRE                   & -          & 3.06          \\
				\hline
				CJT LSTM     &  \textbf{9.65}  & \textbf{0.89}   \\
				\hline
			\end{tabular}
		}
	\end{center}
\end{table}

相关实验结果如表~\ref{tab:collr} 所示。首先，SAT ASR系统超越了ASR基线系统，证明了条件学习的有效性。
然而，PAT SRE系统的表现则比两个SRE基线系统差。
我们认为其原因是ASR基线系统所提供音素信息的准确度
不高所致(ASR基线的WER已超过10\%)。
更重要的是，CJT LSTM系统在ASR和SRE两个任务中均取得了最好的性能表现。
这表明CJT LSTM在训练过程中，SRE模型所反馈的说话人信息促进了ASR模型的训练，
使ASR模型学习到更具有音素区分性的特征，提升了ASR系统的识别性能；
同样地，ASR模型所反馈的音素信息促进了SRE模型的训练，
使SRE模型学习到更具有说话人区分性的特征，提升了SRE系统的识别性能。
因此，这种基于信息即时反馈的协同联合训练机制对于多任务的特征学习是有效的。

\subsection{信号分解}

语音信号中夹杂着各种信息，例如发音内容、说话人特性、情绪、信道和背景噪音等等。
研究者们历经数十年的努力来解码这些信息，先后提出了一系列语音信号处理任务~\cite{benesty2007springer}，
如语音识别、说话人识别、情感识别等。
随着研究地不断深入，某些任务现已得到了很好的解决，如语音识别(ASR)、说话人识别(SRE)。
当然，这在很大程度上要归功于这些任务具备了大量完整标注的语音数据。
然而，对很多任务来说，其仍难以解决，如情感识别(AER)~\cite{el2011survey}。
其中一个主要难点是：由于语音信号中的各种信息混杂在一起，
因此当提取某一种信息时，必然会受到其它信息的干扰。
为此，研究者们尝试对语音信号进行分解，将各种信息分离出来。
例如，在说话人识别中，通过联合因子分析(JFA)~\cite{kenny2007joint}的方法将语音信号
分解成发音内容、说话人和信道三个子空间，并实现了对说话人信息的抽取。
尽管取得了不错的效果，但这些传统方法通常受限于各种先验假设(如线性、高斯)，
使训练得到的模型泛化能力有限。

然而，与传统方法不同，本章所提出的PAT CT-DNN模型则是利用深度神经网络强大的特征学习能力，
基于条件学习机制，实现了语音信号的深度分解。
该模型首先基于大量已标注的语音识别数据，利用深度神经网络从语音信号中学习出用于描述发音内容的音素特征；
然后将这些音素特征作为先验知识，用于说话人特征学习。
因此，基于这种循序渐进地学习模式，我们从语音信号中依次分解得到了具有短时区分性的音素特征和说话人特征。
受PAT CT-DNN模型的启发，本节我们提出了一种基于\textbf{级联学习}的深度分解
(Cascaded deep factorization, CDF)方法，实现对语音信号的深度分解。
该级联深度分解(CDF)的基本思想是秉着由主到次、循序渐进的准则，
优先从主要的、数据充分的任务中学习出与该任务相关的信息；
而后将这些信息作为先验条件，辅助指导相对次要、数据不足的任务的特征学习；
通过逐层的学习，逐渐实现语音信号的深度分解。

该CDF方法与传统JFA等方法有着本质的区别：
1. CDF是基于短时的信号分解，其得到的是帧级别的特征；而JFA是基于长时的信号分解，其得到的是句子级别的表示。
2. CDF是一种区分性模型；而JFA是一种概率统计模型。
3. CDF对不同训练任务可以采用不同的训练数据，因此其数据标注是独立的；
而JFA需要训练数据中同时包含各个训练任务的标注，因此其数据标注是联合的。
4. CDF具有深层、非线性和弱高斯假设的性质；而JFA则是浅层、线性和强高斯假设的。

此外，语言学家发现~\cite{fujisaki1998communication,sagisaka2012computing}，人类对语音信号
的编码和解码过程是逐层渐进的，其优先处理更为主要的语言信息(如语音内容)，其次是副语言信息(如说话人信息)，最后是非语言信息(如情感信息)。
因此，这种级联深度分解方法与人类对语音信号的编码和解码过程十分吻合。

\subsubsection{模型设计}

为了更清楚地描述级联学习方法，我们将其用于情感语音的信号分解中，
如图~\ref{fig:cascade} 所示。
首先，基于语音信号中的发音标注，训练得到一个语音识别系统，并从中提取描述发音内容的音素特征；
其次，将音素特征和原始声学特征相结合，训练得到一个说话人识别系统，并从中提取具有说话人区分性的特征；
最后，将音素特征、说话人特征和原始声学特征相结合，训练得到一个情感识别系统，并从中
提取情感相关的特征。
显然，通过这种级联学习的机制，语音信号被分解成音素、说话人和情感三种因子。
借鉴于JFA的命名，我们将学习到的与某一任务相关的各种信息(或特征)统称为一种\textbf{因子}。

\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\linewidth]{cascade}
	\caption{基于CDF方法的情感语音信号分解}
	\label{fig:cascade}
\end{figure}

为了验证该级联学习机制的有效性，我们提出了基于音素、说话人和情感三种因子的语音频谱重构。
假定音素因子为$q$、说话人因子为$s$、情感因子为$e$，我们尝试用这三种因子对每一帧语音的频谱进行重构。
从时域卷积(对应于对数频域上的加和)的角度，对语音频谱的重构可表示为：

\begin{equation}
\label{eq:rec}
ln (x) = ln \{f(q)\} + ln \{g(s)\} + ln \{h(e)\} + \epsilon
\end{equation}

其中，$f$，$g$和$h$分别为三个非线性重构函数，每个重构函数用一个深度神经网络(DNN)来实现；
$\epsilon$ 为重构残差。图~\ref{fig:recovery} 给出了频谱重构的模型结构。
其中，所有谱均是对数域的。


\begin{figure}[!htp]
	\centering
	\includegraphics[width=0.75\linewidth]{recovery}
	\caption{基于音素、说话人和情感三种因子的频谱重构}
	\label{fig:recovery}
\end{figure}

\subsubsection{实验分析}

在本实验中，我们共选用了三个数据库。为了保证数据格式的一致性，
全部语音数据均统一为8kHz采样率，16bits采样精度。

\textbf{语音识别(ASR)系统}：选用英文 \emph{WSJ} 数据库训练ASR系统，
其数据组成与~\ref{cha:joint} 节一致。
ASR系统共有$4$个隐藏层，每层有$1,024$个结点。
模型输入为$40$维Fbanks特征，考虑上下文各$5$帧，共计$440$维；输出共有$3,383$个结点(等同于senones个数)。
ASR基线系统的词错误率(WER)为9.16\%。
本实验中，我们选用$42$维音素级的后验概率作为音素因子。		
	
\textbf{说话人识别(SRE)系统}：选用英文 \emph{Fisher} 数据库训练SRE系统，
其数据组成和系统配置与~\ref{cha:exp-basic} 节一致。
唯一不同的是，为了降低所学说话人特征的维度，隐藏层中每$500$个结点进行组归一化，
使得最后学习到的说话人特征(说话人因子)为$40$维。
同样地，SRE系统分别选用了基础CT-DNN模型和PAT CT-DNN模型，
且实验结果与~\ref{cha:exp-pat} 节基本一致，此处不再赘述。
	
\textbf{情感识别(AER)系统}：选用 \emph{CHEAVD} 数据库~\cite{bao2014building}训练AER系统。
该数据库取自中文电影与电视节目，并作为标准数据库用于MEC 2016评测~\cite{li2016mec}中。
该数据库中共包含$8$种情感，分别是高兴、生气、惊讶、厌恶、自然、担忧、焦虑和悲伤；
其中，训练集有$2,224$条语音，测试集有$628$条。
AER系统共有$6$个隐藏层，每层有$200$个结点，基于组归一化($p$-norm)降维至$40$维。
模型输入为$40$维Fbanks特征，考虑上下文各$4$帧，共计$360$维；输出结点为对应训练集中的$8$种情感。
模型训练完成后即可得到帧级别的情感后验概率。

我们分别将音素因子(+ ling.)、说话人因子(+ spk.)和二者结合因子(+ ling. \& spk.)作为AER训练的学习条件。
实验结果采用识别正确率(ACC)和宏平均准确率(MAP)来度量。
其中，ACC代表着测试语音被正确识别为真实情感类别的个数占总测试语音数的比例；MAP则是每种情感ACC的平均值。
表~\ref{tab:cdf} 给出了不同情感识别系统在帧级别下的性能对比
\footnote{帧级别的后验概率可通过合并平均的方式得到句子级别的表示；实验表明句子级别与帧级别的评测结果基本一致。}。
可见，在AER模型的训练过程中先验地引入音素/说话人因子将有利于学习更具有情感区分性的情感因子，
这也验证了该级联学习机制的有效性。

\begin{table}[htp]
	\begin{center}
		\caption{不同情感识别系统的性能对比}
		\label{tab:cdf}
		\resizebox{0.70\linewidth}{!}{
			\begin{tabular}{|l|p{1.5cm}<{\centering}|p{1.5cm}<{\centering}|p{1.5cm}<{\centering}|p{1.5cm}<{\centering}|p{1.5cm}<{\centering}|}
			\hline
			\multirow{2}*{\diagbox[width=8em]{测试系统}{测试结果}} & \multicolumn{2}{c|}{训练集}  & \multicolumn{2}{c|}{测试集} \\\cline{2-5}
     				             & ACC(\%)    & MAP(\%)  & ACC(\%)   & MAP(\%)   \\
				\hline
				Baseline         & 74.19 & 61.67 & 23.39 & 21.08           \\
				+ling.           & 86.34 & 81.47 & 27.25 & 27.68           \\
				+spk.            & 92.56 & 90.55 & 27.18 & 28.99           \\
				+ling. \& spk.  & \textbf{94.59} & \textbf{92.98} & \textbf{27.32} & \textbf{29.42}   \\
				\hline
			\end{tabular}
		}
	\end{center}
\end{table}


此外，我们实现了基于公式(~\ref{eq:rec}) 的频谱重构，
三种信息因子通过图~\ref{fig:recovery} 的网络结构对每一帧语音频谱进行恢复。
在训练过程中，基于最小均方差(MSE)的重构损失在验证集上由$15,286$降至$193$。这表明三种信息因子能够较好地实现语音信号的频谱重构。
在此基础上，我们绘制了三种因子在信号分解与重构过程中的频谱，如图~\ref{fig:recovery-2} 所示。

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\linewidth]{recovery-2}
	\caption{基于三种信息因子的语音频谱重构}
	\label{fig:recovery-2}
\end{figure}

实验表明由级联深度分解(CDF)模型学习到的三种信息因子能够对原始语音信号实现高质量的恢复，
这一方面表明了通过CDF模型学习到的信息因子是完备的，验证了基于CDF实现信号分解的可行性；
另一方面也验证了该级联学习机制有利于对次要的、数据不足的任务更好地实现特征学习。

\section{小结}

考虑到说话人特征在学习过程中完全依赖于复杂的模型结构和大量的语音数据，
这种“盲目”的数据驱动使得网络在训练过程中极易受到发音内容等信息的干扰。
为此，本章受条件学习的启发，在模型训练中先验地引入音素条件，
使特征在学习过程中得到了音素知识的补偿，解决了因发音内容不同而导致的说话人特征发散的问题，
进一步提升了所学说话人特征的表征能力。
实验表明，与基础CT-DNN模型相比，基于PAT CT-DNN模型的说话人识别系统在不同测试条件下取得了一致的性能提升。
此外，本章在基于条件学习的PAT CT-DNN模型的基础上，又开展了相关扩展性研究，
先后提出了协同联合训练方法和级联深度分解模型，分别实现了多任务的协同学习和语音信号的深度分解。
