\chapter{绪论}
\label{cha:intro}

\section{说话人识别概述}

\subsection{基本概念}
\label{sec:concept}

语音信号作为一种非接触性信息载体，其在形式简单的一维信号中蕴含着丰富的信息(“形简意丰”)，包括语言信息(如语音内容)、副语言信息(如音高、音量、语调等)以及非语言信息(如健康状况、性别、年龄、环境背景等)~\cite{benesty2007springer}。
声纹作为语音信号中的一种信息，
是对语音信号中所蕴含的能表征说话人身份的语音特征以及基于这些特征所建立的语音模型的总称~\cite{vpr2008}。
由于不同说话人在讲话时所使用的发声器官(如舌头、口腔、鼻腔、声带、肺等)在尺寸和形态等方面均有所不同，
再考虑到不同说话人在年龄、性格、语言习惯等因素上的差异，
使得不同说话人的发音容量和发音频率等特性大不相同。
可以说，任何两个人的声纹图谱都不尽相同~\cite{吴朝晖2009说话人识别模型与方法}。

说话人识别(SRE)，又称声纹识别 (VPR)，是根据语音信号中能够表征说话人个性信息的声纹特征，
利用计算机以及各种信息识别技术，自动地实现说话人身份识别的一种生物特征识别技术
~\cite{lass2012contemporary,campbell1997speaker,zheng2017robustness}。
与其他生物特征识别相比，说话人识别继承了语音信号的以下特点：

1. 语音信号是可双向传递的，既可接收信息，也可发出信息，这使得说话人识别易于实现人机交互、体验性更好；

2. 语音信号作为一种非接触式信息载体，其采集成本低廉、使用简单，这使得说话人识别易于实现远程身份认证；

3. 语音信号是高可变性与唯一性的完美统一，说话人在不同时刻所说的话是完全不同的，
但语音信号中所蕴含的说话人信息却又是唯一确定的，这使得说话人识别具备了很强的防攻击能力。

说话人识别主要由训练和识别两个阶段组成，下图~\ref{fig:1} 是一个基本的说话人识别系统框架~\cite{campbell1997speaker,kinnunen2010overview}：

1. 训练阶段：首先对使用系统的说话人预留充足的语音，并提取该语音中的声纹特征，
然后根据说话人的声纹特征训练得到说话人模型，最后将全部说话人模型构成系统的说话人模型库。

\begin{figure}[H]
	\centering
	\includegraphics[width=1\linewidth]{1}
	\caption{一个基本的说话人识别系统框架}
	\label{fig:1}
\end{figure}

2. 识别阶段：说话人在进行识别认证时，系统对待识别语音进行与训练阶段相同的声纹特征提取过程，
并将声纹特征与说话人模型库进行比对，得到对应的相似性打分，
最后根据相似性打分判决待识别语音的说话人身份。

\subsubsection{说话人识别的分类}
\label{sec:classfication}

从不同的分类角度看，说话人识别可大致分为以下几类~\cite{campbell1997speaker,furui1997recent,zheng2017robustness}。

1. 说话人辨认和说话人确认

从实际应用的范畴，说话人识别可分为说话人辨认和说话人确认。
说话人确认是确定待识别语音是否来自其所声称的目标说话人，是一个“一对一”的判决问题；
说话人辨认是判定待识别语音属于目标说话人集合中哪一个说话人，是一个“多选一”的选择问题。
此外，根据测试范围的不同，说话人辨认又可划分为闭集辨认和开集辨认。
闭集辨认是指待识别语音必定属于目标说话人集合中的某一个说话人；
而开集辨认是指待识别语音不受限于目标说话人集合，其可属于该集合外的某一位说话人。

除此之外，在实际应用中，说话人识别还涵盖了说话人检测 (即检测目标说话人是否在某段语音中出现) 
和说话人追踪 (即以时间为索引，实时检测每段语音所对应的说话人)~\cite{tranter2006overview}等。

2. 文本相关、文本无关和文本提示

从发音文本的范畴，说话人识别可分为文本无关、文本相关和文本提示 三类~\cite{campbell1997speaker,kinnunen2010overview,zheng2017robustness}。
文本无关是指说话人识别系统对于语音文本内容无任何要求，说话人的发音内容将不受任何限制，只要语音达到一定时长即可；
而文本相关则要求用户需按照预先指定的固定文本内容进行发音。
对比这两类说话人识别，文本相关的说话人识别的文本内容匹配性明显优于文本无关的说话人识别，
所以一般来说其系统性能也会相对好很多。
但是，文本相关对说话人预留和识别时的语音录制有着更为严格的限制，并且相对单一的识别文本更容易被窃取。
相比于文本相关，文本无关的说话人识别使用起来更加方便灵活，具有更好的体验性和推广性。
为此，综合二者的优点，文本提示型的说话人识别应运而生。
对文本提示而言，系统从说话人的训练文本库中随机地抽取组合若干词汇，作为用户的发音提示。
这样不仅降低了文本相关所存在的系统闯入风险，提高了系统的安全性，而且实现起来也相对简单。

\subsubsection{说话人识别的性能评价}

根据说话人识别任务的不同，其系统性能的评价指标也略有不同~\cite{zheng2017robustness}。
对于说话人确认系统，通常采用检测错误权衡曲线(DET)、等错误率(EER)和检测代价函数 (DCF)；
而说话人辨认系统则根据测试集合的不同，选择不同的系统评价指标。

1. 说话人确认系统性能指标

说话人确认系统的性能评价主要依据两个参量，分别是错误接受率 (FAR) 和错误拒绝率 (FRR)。
FAR 是指将非目标说话人误判为目标说话人而产生的错误。FRR 是指将目标说话人误识成非目标说话人而产生的错误。
在说话人确认系统中，可通过设定不同的阈值对 FAR 和 FRR 进行权衡。
一般采用检测错误权衡 (DET) 曲线~\cite{martin1997det}来反映两个错误率之间的关系：
对一个特定的说话人确认系统，以 FAR 为横坐标轴，
以 FRR 为纵坐标轴，通过调整其阈值参数得到的 FAR 和 FRR 之间的关系曲线图就是 DET 曲线。
在 DET 曲线上，第一象限的角平分线与其交点之处的 FAR 和 FRR 值相等，该交点所对应的错误率称为等错误率(EER)。
显然，EER 值越小系统性能相对越好，它代表了说话人确认系统的一个整体性能，
是衡量系统性能的重要参数。

在美国国家标准技术研究所(NIST)所组织的评测中~\cite{doddington2000nist}，
还定义了 FAR 和 FRR 的加权和函数，即检测代价函数 (DCF) 作为系统性能的评价指标。
针对不同的应用场景对 FAR 和 FRR 定义不同的权重，
用由此计算得到的 DCF 值代表系统性能。

2. 说话人辨认系统性能指标

通常情况下，在开集说话人辨认系统中仍可采用等错误率 (EER) 和检测代价函数 (DCF) 作为系统性能的评价指标。

在闭集说话人辨认系统中通常采用正确识别率 (简称为识别率)、错误识别率 (简称为错误率) 以及
前 N 辨认正确率 (Top-N IDR) 作为评价系统性能的指标。
识别率是指待识别语音从目标说话人集合中正确地找出所对应真实说话人的比率。
通常将待识别语音与目标说话人集合中相似度最大的说话人作为辨认说话人，
其辨认正确的比率称为 Top-1 辨认正确率(Top-1 IDR)；
若在目标说话人集合上相似度最大的前 n 个说话人中包含真实说话人即认为辨认正确，
则由此统计出来的辨认正确率称为 Top-n 辨认正确率(Top-n IDR)。

\subsubsection{说话人识别发展历史}
\label{sec:history}

“闻其声而知其人”，通过人耳听觉感知来辨别声音中的说话人身份，古已有之。
下图~\ref{fig:2} 总结了说话人识别技术的发展历史~\cite{吴朝晖2009说话人识别模型与方法,zheng2017robustness}。

\begin{figure}[H]
	\centering
	\includegraphics[width=1\linewidth]{2}
	\caption{说话人识别技术的发展历史}
	\label{fig:2}
\end{figure}

以语音作为身份认证的手段，最早可追溯到17世纪60年代英国查尔斯一世之死的案件审判中。
1945年，Bell实验室的L. G. Kesta等人借助肉眼观察，完成语谱图匹配，并首次提出了“声纹”的概念；
并随后在1962年第一次介绍了采用此方法进行说话人识别的可能性。
随着研究手段和计算机技术的不断进步，说话人识别逐步由单纯的人耳听辨转向基于计算机的自动识别。
从语音信号处理的角度，研究者们提出了倒谱~\cite{atal1971speech}、
共振峰~\cite{doddington1972automatic}、基频轮廓~\cite{atal1972automatic}等特征，
并将其应用于说话人识别中，取得了不错的效果。

从20世纪70年代至80年代，有效的声学特征参数和模式匹配方法成为说话人识别的研究重点。
研究者们相继提出了线性预测编码(LPC)~\cite{makhoul1976lpcw}、
线谱对(LSP)~\cite{zheng1998distance,sahidullah2010use}、线性预测倒谱系数(LPCC)~\cite{atal1976automatic}、
感知线性预测(PLP)~\cite{hermansky1990perceptual}、梅尔频率倒谱系数(MFCC)~\cite{vergin1999generalized}等
一系列声学特征参数。
与此同时，动态时间规整(DTW)~\cite{sakoe1978dynamic}、
矢量量化(VQ)~\cite{burton1983generalization}、
隐马尔科夫模型(HMM)~\cite{schuster2007introduction}等已在语音识别领域得到广泛运用的技术，
也逐渐成为说话人识别的重要技术。

20世纪90年代以来，尤其是D. Reynolds对高斯混合模型(GMM)~\cite{reynolds2015gaussian}做了详细介绍后，
基于最大似然的概率统计模型GMM迅速成为了文本无关说话人识别中的主流技术，将说话人识别研究带入了一个新的阶段。
2000年，D. Reynolds在说话人确认任务中提出了高斯混合模型-通用背景模型(GMM-UBM)结构~\cite{reynolds2000speaker}，
为说话人识别技术从实验室走向实用作出了重要贡献。

进入21世纪，在传统GMM-UBM方法上，P. Kenny、N. Dehak等人先后提出了联合因子分析(JFA)~\cite{dehak2007modeling}
和i-vector模型~\cite{dehak2011front}，
将说话人模型映射到低维子空间中，得到了一个低维的说话人向量表示。
在i-vector模型后端还可以通过类内协方差归一化(WCCN)~\cite{hatch2006within}、
扰动属性投影(NAP)~\cite{solomonoff2004channel}、线性判别分析(LDA)~\cite{dehak2011front,mclaren2011source}
、概率线性判别分析(PLDA)~\cite{ioffe2006probabilistic,kenny2010bayesian}等方法，
进一步去除与说话人无关的会话信息，从而提高了i-vector对说话人的区分能力。
近年来，随着深度学习在语音识别等语音信号处理领域的快速发展和成功应用，基于深度学习的相关方法~\cite{lei2014novel,kenny2014deep,variani2014deep,zhang2016end,snyder2016deep}也逐渐应用到说话人识别中，并取得了不俗的效果。

\subsection{应用和挑战}
\label{sec:application}

\subsubsection{说话人识别的应用}

历经数十年的发展，说话人识别技术已逐步从小规模的实验室环境应用到大规模的实际场景中。
目前，说话人识别技术已被广泛应用于军事、国防、政府、金融等不同领域。
根据实际应用范畴，本节将从说话人辨认和说话人确认等方面介绍说话人识别技术的应用情况~\cite{zheng2016}。

1. 说话人辨认技术的应用

说话人辨认技术现已广泛应用于公安司法、军事国防等领域中，举例如下：

(1). 国防安全

在通信系统或安全监测系统中预先安装说话人辨认系统，可采用通讯跟踪和说话人辨别技术对罪犯进行行为监控和侦查追捕。
在人口密集、流动大的公共场所，可采用说话人辨认系统有效地对危险人物进行鉴别和提示，
降低人工识别所带来的疏漏，更好地保证人们生命财产的安全性。

(2). 公安技侦

近年来，通过电话勒索、绑架等刑事犯罪案件时有发生。
为此，利用说话人辨认技术，公安机关可以从通话语音中锁定嫌疑犯人，缩小刑侦范围。
此外，该技术用于对满刑释放的犯罪嫌疑人进行监听和跟踪，可有效防止犯罪嫌疑人再次犯科，也有利于对其进行及时抓捕。

2. 说话人确认技术的应用

随着互联网的快速发展，远程身份认证的安全性亟待加强。
说话人确认技术可以满足远程身份认证的安全性需求，现已广泛应用于电子支付、声纹锁控、社保等领域中。

(1). 电子支付

2014年中国互联网支付用户调研报告显示，网上支付、手机支付、第三方支付已成为人们购物付款的主流方式。
为了保障电子支付的安全性，将说话人确认技术应用其中，通过动态密码口令等形式进行个人身份认证，
有效地提高了个人资金和交易支付的安全性。

(2). 声纹锁控

近年来，数以万计的腾讯 QQ 用户出现了账号被盗取的情况。
盗号者通过联系用户的亲朋好友进行金钱诈骗，给用户及其亲友带来了严重损失。
通过采用声纹认证代替明文密码认证，提高了用户账号的安全性，有效地避免此类事件再次发生。

(3). 社保

为了防止养老金被冒领，社保局可通过预装说话人确认系统，再结合人工辅助手段，
对养老金领取者进行现场身份认证，或者当本人无法亲临现场时通过电话进行远程身份确认，
有效地制止了国家社保养老金的流失，提高了社保服务机构的工作效率。

3. 其它应用领域

除了上述相关应用领域，说话人检测和追踪技术也有着广泛地应用。
在含有多个说话人的语音段中，如何准确高效地把目标说话人检测标识出来有着十分重要的意义。
例如，在现有音频/视频会议系统中，通常设有多麦克风阵列用以实时记录会议中每一个说话人的讲话。
通过将说话人追踪技术嵌入该会议系统，可实时标识和追踪每段语音所对应的说话人。
该技术解放了人为会议纪要的繁琐，提高了工作效率。

\subsubsection{说话人识别的挑战}

说话人识别的广泛应用与其技术的发展进步是息息相关的。
近年来，在限定条件下的说话人识别已取得了令人满意的系统性能
~\cite{campbell1997speaker,furui1997recent,kinnunen2010overview,hansen2015speaker}。
然而在实际应用中，说话人识别系统受各种不确定性因素的制约，其系统鲁棒性面临了巨大的挑战。
本节将总结当前说话人识别所面临的若干挑战。

1. 非限定文本

当前主流的说话人识别系统大都是基于概率统计的产生式模型。
因此，在非限定文本 (文本无关) 的条件下，通常需要充分时长的语音数据进行说话人的建模与识别，
以此弥补训练语音和测试语音在发音空间上的不一致性。
在实际应用中 (如电子支付、 门锁控制等)，长时的语音预留与测试将极大地降低用户体验性；
在某些场景中甚至无法获取足够时长的语音(如刑侦安防)~\cite{zhangch2014}。
因此，如何在非限定文本的条件下，尽可能地避免语音时长的限制具有很大的研究意义。

2. 背景噪音

在实际应用中，除了说话人的声音外，语音信号中还混杂着各种各样的背景噪音，如白噪音、汽车噪音、音乐噪音等等。
一方面，在说话人模型训练时，这些背景噪音将会混杂在说话人模型中，降低说话人模型的‘纯度’；
另一方面，其会对说话人的识别认证造成混淆和干扰，降低说话人识别的系统性能。
更重要的是，这些背景噪音通常是不可预知的，这使得其对说话人识别系统的影响具有很大的不确定性。
因此，如何更好地消除背景噪音的影响一直是国内外的研究热点和难点~\cite{drygajlo1998speaker,ming2007robust}。

3. 信道失配

在实际应用中，语音信号可通过各式各样的采集设备录制得到，如手机麦克风、固定电话、采访录音笔等等。
此外，语音信号也可通过不同的传输途径发送至说话人识别系统，如固话传输、网络传输、扩频传输等。
因此，语音信号中既包含了说话人信息，也包含了信道信息。
这些信道信息会使原始语音信号发生频谱畸变，影响了声纹特征对说话人的表征能力，
从而降低了说话人识别系统的性能~\cite{reynolds2003channel,dehak2011front}。

4. 说话人自身

一个说话人的声音虽相对稳定，但仍具有易变性。

(1). 身体状况：说话人由于身体状况的变化，如感冒、喉炎、鼻塞及其它原因，
引起发音变化，导致说话人识别的准确率降低~\cite{tull1996analysis,tull1996cold}。

(2). 时间变化：人的声道会随着年龄的增长而变化，因此同一个人在不同年龄段所发出的声音也是有所不同的。
当说话人的预留建模与测试识别的时间间隔超过一定限度时，
说话人识别系统的性能会明显衰减~\cite{wang2012investigation,wangll2013}。

(3). 情绪波动：语音信号中携带着情感信息，同一个人在不同情感下所发出的语音也是有所不同的。
情绪波动会对音量、语速、语调等产生影响，
导致说话人识别的准确率降低~\cite{bie2013emotional,zetterholm1998prosody}。

因此，如何解决说话人自身的不确定性也是说话人识别的一个研究难点。

\section{选题背景}

在~\ref{sec:application} 节中提到，语音信号的不确定性对说话人识别系统提出了巨大的挑战。
为此，许多国内外高校、科研机构和公司企业陆续开展了一系列研究，探索如何降低这种不确定性对说话人识别的影响，
提高系统的鲁棒性。
总体上看，当前说话人识别的研究可归纳为两个方向：基于特征的识别方法和基于模型的识别方法。
前者从特征域上，挖掘对说话人特性敏感而对非说话人因素鲁棒的特征；
后者从模型域上，构建概率统计模型，将语音信号分解为说话人因子和非说话人因子，实现对说话人特征的统计建模。
本节我们首先简要地分析说话人识别在特征域和模型域上的研究现状，
然后综述基于深度神经网络的特征学习，
最后引出本文的选题目标：说话人识别中的特征学习。

\subsection{研究现状}
\label{sec:current}

\textbf{1. 基于特征的识别方法}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.75\linewidth]{4}
	\caption{语音产生与语音感知~\cite{reetz2011phonetics}}
	\label{fig:4}
\end{figure}

基于特征的识别方法的基本思想是：挖掘语音信号中对说话人特性敏感而对非说话人因素不敏感的特征。
从模式识别的角度来看，如果能够找到一个有效的特征，那么可以大大简化后端模型的复杂度，使得系统具有更强的鲁棒性和可扩展性。
从科学认知的角度来看，说话人特征提取与选择的过程能够更好地帮助人类理解描述说话人特性的信息是如何嵌入在语音信号中的。
为此，研究者们从语音产生和语音感知等角度，
参照人类听辨说话人的方式，致力于寻找可以描述说话人 “基本特性” 的特征~\cite{kinnunen2010overview,hansen2015speaker}。

图~\ref{fig:4} 给出了语音产生和语音感知的过程~\cite{reetz2011phonetics}。
在讲话时，说话人的各种发音器官(如肺、喉和声道)通力协作，将描述说话人特性的信息编码在语音信号中；
在听辨时，听音人的各种听觉器官(如外耳、中耳和内耳)分层解码语音信号中的各种信息，并将其传递给大脑。
为此，研究者们基于不同的发音机理与听觉机理，关注于不同的尺度单元，利用不同的变换工具，得到了属性各异的特征。
总体上看，这些特征可分为以下几种：


(1). 短时频谱特征:
基于声道的共振规律和语音信号的短时平稳假设，对语音信号进行加窗、分帧，计算得到每一帧语音的频谱特征。
常见的短时频谱特征有：线性预测倒谱系数(LPCC)~\cite{atal1971speech}、梅尔频率倒谱系数(MFCC)~\cite{vergin1999generalized}、
感知线性预测(PLP)~\cite{hermansky1990perceptual}等。

(2). 声源特征:
声源特征描述了声门激励的特点，包括声门脉冲形状和基音频率等。
研究者认为这些特征中携带了说话人相关的信息~\cite{plumpe1999modeling}。
常见的声源特征有：线性预测分析、相位特征~\cite{murty2006combining,vijayan2016significance}等。

(3). 时序动态特征：
时序动态特征所描述的是语音信号的动态特性，例如共振峰的变化、能量的调节等。
常见的时序动态特征有：短时频谱特征的一阶差分或二阶差分($\Delta$、$\Delta\Delta$)~\cite{soong1988use,huang2001spoken}、
其它长时动态特征~\cite{kinnunen2006joint,thiruvaran2008fm}等。

(4). 韵律特征：
与短时频谱特征不同，韵律是对语音段的描述；该语音段可以是音节、词、句子等。
韵律描述的是语音信号中的音节重音、语调、语速和节奏等~\cite{shriberg2005modeling,adami2007modeling}。
常见的韵律特征有：基频~\cite{rose2003forensic}、时长信息等。

(5). 语言学特征：
每个说话人拥有其独特的发音词表和个人习语。
这些高层特征通常作为辅助信息用于说话人识别中。
常见的语言学特征有：音素、词的分布规律等~\cite{doddington2001speaker,campbell2004phonetic,leung2006adaptive}。

上述特征借鉴了人类在听辨说话人身份时的处理方式，可视为“知识驱动”的特征。
这些特征在特定领域、特定数据库的说话人识别任务中取得了一定效果，但其普适性仍十分有限。
例如，高层语言学特征很容易受发音人的情绪和场景的变化而发生改变；
短时频谱特征中通常还包含了信道、噪声、发音内容等复杂信息，引入了各种不确定性。
因此，很多研究者转而研究基于模型的识别方法，通过设计合理的概率模型来描述这些特征中的不确定性，
从而得到每个说话人的统计特性，并基于这些统计特性对说话人进行识别。

\textbf{2. 基于模型的识别方法}

当前主流的说话人识别系统大都是在模型域上开展的，
其基本思想是：构建一个概率统计模型用于描述说话人因子与非说话人因子之间的关系；
当该模型训练完成后，与说话人相关的因子便可从语音信号中预测出来。

其中，高斯混合模型-通用背景模型(GMM-UBM)是一个经典的说话人识别模型~\cite{reynolds2000speaker,campbell2006support}。
高斯混合模型(GMM)是由若干个多维高斯密度函数经过线性加权组成的一个整体分布。
通常，多个高斯概率分布的线性组合可逼近于任意的分布；
因此，GMM可相对准确地描述语音特征的分布情况。

\begin{figure}[H]
	\centering
	\includegraphics[width=0.45\linewidth]{3}
	\caption{基于MAP的GMM-UBM模型~\cite{kinnunen2010overview}}
	\label{fig:3}
\end{figure}

基于GMM-UBM的说话人识别框架可分为三个部分~\cite{reynolds2000speaker}。

第一，利用来自不同说话人的大量语音数据建立一个相对稳定且与说话人特性无关的高斯混合模型(GMM)。
该模型描述了不同说话人在声学空间中的共享特性，被称为通用背景模型(UBM)。
该模型将整个声学空间划分成若干个声学子空间(即为若干个UBM混合分量)；
每个声学子空间是一个与说话人无关的高斯分布，粗略地代表了一个发音基元类。
如图~\ref{fig:3} 中的蓝色实线所示。

第二，基于最大后验估计算法(MAP)~\cite{bilmes1998gentle}，利用说话人的语音数据在UBM上自适应得到该说话人的GMM。
该说话人的每个声学子空间(即为一个GMM混合分量)由一个说话人相关的高斯分布所描述；
而该说话人相关的高斯分布是由与其对应的说话人无关的高斯分布通过MAP自适应得到。
如图~\ref{fig:3} 中的红色虚线所示。

第三，在测试阶段，计算待测试语音的声学特征在目标说话人模型(GMM)和通用背景模型(UBM)上的
对数似然比(LLR)作为系统的判决打分。

考虑到大多数情况下，我们只对每个高斯分量的均值向量进行自适应~\cite{reynolds2000speaker}，
因此，事实上我们可以将GMM-UBM抽象成一个线性因子分解模型。
语音信号$x \in R^d$ 被分解成一个语言因子$\mu_z \in R^d$ 和一个说话人因子$w_z \in R^d$，其公式可表示如下：

\begin{equation}
\label{equ:gmm}
x = \mu_z + D w_z + \epsilon_z
\end{equation}

\noindent 其中，$z$ 是每个高斯分量的索引，其服从多项分布；$D$ 是一个等距对角矩阵；
语言因子$\mu_z$ 对应的是UBM中第$z$个高斯分量的均值向量；$\mu_z + D w_z$ 则是说话人GMM第$z$个高斯分量的均值向量；
说话人因子$w_z$服从$N(\textbf{0}, \textbf{I})$的高斯分布；
$\epsilon_z \in R^d$是服从$N(\textbf{0}, \mathbf{\Sigma}_z)$的残差。
因此，GMM-UBM的本质是基于最大似然(ML)准则的线性因子分解模型，其将语音信号分解成语言因子、说话人因子和残差因子。

随后，在GMM-UBM的基础上，研究者们尝试将表征说话人特性的因子映射到一个低维子空间中，扩展出一系列说话人因子的低维表示模型。
例如，联合因子分析(JFA)模型~\cite{kenny2007joint}将语音信号分解为三个因子：语言因子、说话人因子和会话因子；
其中，说话人因子和会话因子都是低维的。
i-vector模型~\cite{dehak2011front}是JFA模型的简化表示，其采用单一的“全变量因子”同时表述说话人因子和会话因子；
并依赖于后端区分性模型(如PLDA模型~\cite{kenny2010bayesian,ioffe2006probabilistic})来实现对说话人因子的“提纯”。
基于深度神经网络-语音识别(DNN-ASR)的i-vector模型遵照同样的准则，采用基于深度神经网络训练的语音识别模型替换
基于最大期望(EM)算法~\cite{bilmes1998gentle}训练的UBM，以此获取更精确的语言因子，
进而预测出更准确的说话人因子。

上述这些模型通常需预先定义各个因子之间的概率依附关系。为了简化训练和预测的复杂度，大多数模型需服从线性、高斯的假设。
事实上，语音信号中各个因子之间的关系是错综复杂的。
因此，这类模型难以准确地描述语音信号中各个因子之间复杂的相互关系，使得预测出的说话人因子仍存在很大的缺陷。

\subsection{基于深度神经网络的特征学习}
\label{sec:learning}

基于统计模型的说话人识别方法虽取得了极大成功，
然而，受各种不确定性 (如非限定文本、跨信道、环境噪音、说话方式等) 的制约，
当前的说话人识别系统仍难言可靠。其中一个主要原因是这一方法基于原始特征(如MFCC)和线性高斯模型(如GMM-UBM, i-vector)。
原始特征受各种非说话人因素的影响显著、变动性强；而线性高斯模型本身的先验假设过强，难以有效地描述这些变动性。
为解决这一问题，一个可行的办法是寻找具有更强不变性的说话人特征，使得简单的线性高斯模型足以对其分布进行建模。
然而，传统“知识驱动”的特征设计方法通常基于较强的先验假设，所设计得到的特征泛化能力不足。
因此，我们希望得到一种基于“数据驱动”的特征学习方法：\textbf{给定特征的基本特性，基于任务目标自动地学习出特征的具体形式}。
这一特征学习方法可以避免人为设计的偏颇和疏漏，同时得到的特征具有更强的任务相关性。
当数据足够充分时，这一方法有可能得到“品质”极佳的特征。

特征学习需要一个合理的学习结构，这一结构应具有足够的灵活性，具有结合领域知识的能力，同时也应具有较高的学习效率。
深度神经网络(DNN)是一个具有多层结构的神经网络，其拥有足够强大的函数表达能力(与层数成指数关系)
~\cite{hinton2006reducing,hinton2006fast,delalleau2011shallow,montufar2014number}，
可针对领域知识设计各种灵活的网络结构，
且具有高效的训练方法(如随机梯度下降SGD~\cite{rumelhart1986learning,bottou2004stochastic})。
特别是深度神经网络的层次结构，为特征学习提供了非常有效的载体。
如图~\ref{fig:5} 所示，特征学习通过无监督学习生成层次性特征；
分类模型通过有监督学习完成任务分类与建模。
通过DNN误差信息的反向传播实现了特征学习和分类模型的整体优化，最后得到与任务相关的层次性特征。

\begin{figure}[H]
	\centering
	\includegraphics[width=0.65\linewidth]{5}
	\caption{由层次性特征和分类模型所组成的深度神经网络}
	\label{fig:5}
\end{figure}

层次性是自然界的基本原则。人类大脑对信息处理的过程也是层次性的：相邻层之间互相连接，
后一层接收前一层提供的信息并进行加工处理~\cite{northoff2013unlocking,serre2007quantitative}。
这种层次结构使得人类的神经系统具有了更强的信息表达能力。
一方面，前一层处理得到的信息可被后一层多个神经元复用，节约了计算量；
另一方面，由前一层处理过的信息不必再重复处理，使得后一层可以关注于更高级的信息处理。

深度学习很好地运用了这种层次结构，通过深层神经网络学习得到不同层次性的特征：
在网络浅层可能只是一些原始特征，越往高层越抽象，越具有不变性。
以人脸识别~\cite{lee2009convolutional}为例，深度神经网络(DNN)对人脸特征的学习是分层的。
第一层首先学习一些简单的线条，表达图像中某些位置和方向上的轮廓；
第二层会根据第一层检测出的线条，学习一些局部特征，如眼睛、口鼻等；
第三层则已经学习到大体的人脸轮廓。通过三层网络结构即可从原始充满着各种不确定性的图片中提取出与人脸相关的特征信息。
从直观上看，为了更好地表达数据的特性，网络首先需要选择最具有代表性的特征。
在参数量固定的条件下，学习系统应优先选择那些简单的特征，因为这些特征更容易在表达多种数据模式中被复用，从而提高了
特征的表达能力。因此，网络浅层通常学习的是简单模式。当扩展至深层时，其在浅层简单模式的基础上进行组合，生成抽象模式。
研究表明~\cite{lee2009convolutional,hinton2012deep}，
这种通过深度神经网络来自动学习特征的方式往往比人为设计特征更具有代表性和鲁棒性。


归因于其强大的特征学习能力，深度神经网络(DNN)在图像识别、语音识别、自然语言理解等领域
~\cite{lee2009convolutional,dahl2012context,hinton2012deep}
取得了一系列领人瞩目的成就。
在说话人识别领域，Variani等人~\cite{variani2014deep}在2014年提出了基于深度神经网络的说话人特征学习，
并用于文本相关的说话人识别中。
他们构建了一个DNN模型，以训练集中的496个说话人作为训练目标；帧级别的说话人特征从DNN最后一个隐藏层的激活函数中提取出来；
将帧级别的说话人特征以合并平均的方式得到句子级别的表示(称为‘d-vector’)；
最后通过计算测试语音和预留语音之间d-vectors的余弦距离进行打分判决。
实验表明，该d-vector系统比主流的i-vector基线系统差，但在打分阶段将两个系统融合取得了不错的效果。
在此基础上，我们~\cite{li2015improved}改进了模型后端的打分策略，提出了基于动态时间规整(DTW)~\cite{berndt1994using}的打分方法。
虽在一定程度上提升了d-vector系统性能，但仍与i-vector基线相差甚远。

本论文在Variani等人的工作基础上~\cite{variani2014deep,li2015improved,heigold2016end}，
深入地研究了基于深度神经网络的说话人特征学习方法，在模型结构、目标函数、训练方法
等方面进行了一系列探索，并验证了所学到的说话人特征在各种典型说话人识别应用场景(如短语音、跨语言)中的推广性。

\section{研究工作概述}

\subsection{研究难点}
\label{sec:difficulty}

本文的研究目标是如何利用深度学习方法从语音信号中学习出能够表征说话人信息的特征，构建一套说话人特征学习研究框架。
本文的研究难点主要有以下三个方面：

1. 原则上，神经网络模型具有普适函数近似能力，这意味着只需提供
一个足够自由的网络结构，基于充足的数据和计算资源，即有望实现针对目标任务的特征学习。
然而，这只是一种理想状态，我们通常没有足够的数据来满足模型训练的要求；高复杂度的模型也使参数优化变得极为困难。
为此，我们需要尽可能地引入先验知识，依据语音信号的特性来设计具有足够表达能力的、简洁的、可训练的模型结构。
\textbf{如何设计一个相对合理的基础模型结构，使之能够学习出具有说话人区分性的特征}是所面临的第一个难点。

2. 说话人特征学习的目标是从语音信号中学习出与说话人相关的特征，而并没有直接针对说话人识别任务。
为此，我们需要将由基础模型所学到的说话人特征推广至不同说话人识别任务中，
以此验证所学说话人特征在不同任务场景下的通用性，进而证明基础模型结构的有效性。
\textbf{如何验证所学说话人特征的通用性和基础模型结构的有效性}是所面临的第二个难点。

3. 在验证了所学特征和基础模型的基础上，我们需要进一步分析模型结构中的缺陷和训练方法的不足，
尝试引入与说话人识别任务相关的知识和限制，尽可能地使模型从数据中学习到更具有说话人区分性的特征。
\textbf{如何进一步改进模型结构和训练策略，实现说话人特征的优化与增强}是所面临的第三个难点。

\subsection{研究思路}
\label{sec:thought}

针对本文的研究目标与其所面临的研究难点，本文的研究思路主要分为三个过程：
首先，从语音信号基本特性出发，结合说话人信息在语音信号中的表征形式，
设计适用于说话人特征学习的基础模型结构；
其次，将所学说话人特征应用于各种典型的说话人识别场景中，
特别是跨语言、短语音等具有较强挑战性的场景，以验证说话人特征学习的推广性；
最后，针对说话人识别任务，分析模型结构中的缺陷和训练方法的不足，
进一步研究提高说话人特征表征能力的特征增强方法。

因此，本文对于说话人识别中的特征学习方法研究被分解为三个子问题，
即如何设计合理的特征学习模型结构，如何验证说话人特征学习的推广性以及
如何进一步改进特征学习模型、提高说话人特征的表征能力。
整个研究思路如图~\ref{fig:6} 所示。

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\linewidth]{6}
	\caption{论文研究思路}
	\label{fig:6}
\end{figure}

\subsubsection{特征学习的模型设计}

在~\inlinecite{variani2014deep, li2015improved}等前人工作的基础上，
我们从语音信号自身出发，首先解析语音信号在时频空间中的基本特性；
而后分析这些基本特性对说话人信息的表征形式，
最后设计了能够描述这种表征形式的模型结构。

语音信号是一种短时平稳信号，其兼有局部属性和动态属性。
局部属性是指语音信号在时频空间中具有明显重复的典型模式；
动态属性是指语音信号具有时序相关性。
这些属性表征了语音信号中说话人信息的稳定性与唯一性。

针对说话人信息在语音信号中的表征形式，我们设计了一个能够解析这种表征形式的网络结构。
该网络结构是一个包含了卷积、时延和组归一化的卷积-时延深度神经网络(CT-DNN)。
在这一结构中，卷积模块用来提取语音信号中的典型模式，保证对语音信号局部属性的学习；
时延模块用来引入上下文信息，以学习语音信号的动态属性；
基于$p$-范数的组归一化则用来减小网络规模，提高网络的可训练性。
在此结构基础上，以最大化区分不同说话人为目标函数，分帧对网络进行训练；
每一帧在最后一个隐藏层的输出即为帧级别的说话人特征。

\subsubsection{特征学习的推广性研究}

说话人特征学习的一个重要优势是所学特征可应用于任何一个与说话人相关的任务中，
包括说话人确认、说话人辨认、说话人分割等。
然而，说话人特征学习本身的目标函数并不直接针对上述这些任务。
因此，我们需要验证所学说话人特征在说话人相关任务中的通用性和普适性，以证明该特征学习方法的推广性。
为此，本文将从三个方面来验证说话人特征学习的推广性：
1. 通过比较特征学习方法与面向任务的“端到端”方法，验证特征学习方法对说话人识别任务的推广性；
2. 通过将所学说话人特征应用到跨语言说话人识别中，验证特征学习方法在跨语言场景下的推广性；
3. 通过将所学说话人特征应用到短语音说话人识别中，验证特征学习方法在短语音场景下的推广性。

\subsubsection{模型改进与特征增强}

通过特征学习的推广性研究，我们将验证上述卷积-时延深度神经网络(CT-DNN)可以有效地实现说话人特征学习，
但是该特征学习方法对说话人识别任务本身并没有优化。
对说话人识别而言，说话人的类内内聚性和说话人的类间离散度对说话人识别任务同等重要。
而对特征学习而言，基础CT-DNN模型的训练目标是最大化区分不同说话人，
其只关注于说话人的类间离散度，而忽视了说话人的类内内聚性，使所学到的说话人特征存在类内发散的问题。
因此，为了使学习到的说话人特征对说话人识别任务尽可能地优化，我们需要在模型训练中限制说话人的类内方差，
增强所学说话人特征的类内内聚性。
为此，本文将从两个方面来提升说话人特征的类内内聚性：
一是从模型自身的角度，针对说话人识别任务，分析模型结构中的缺陷，尝试在模型训练中引入与任务相关的限制，
增强所学特征的类内内聚性；
二是从学习方法的角度，受条件学习的启发，尝试在模型训练中先验地引入与任务相关的知识，进一步增强所学特征的表征能力。

\subsection{研究内容}
\label{sec:content}

本文围绕图~\ref{fig:6} 所示的研究思路，针对说话人识别中的特征学习任务，开展了一系列研究工作，
构建了一套基于说话人特征学习的研究框架。研究内容如下图~\ref{fig:structure} 所示：

\begin{figure}[H]
	\centering
	\includegraphics[width=1\linewidth]{structure}
	\caption{说话人特征学习研究框架}
	\label{fig:structure}
\end{figure}

\subsubsection{基于卷积-时延深度神经网络的说话人特征学习}
\label{sec:ct-dnn}

从语音信号的基本特性出发，结合说话人信息在语音信号中的表征形式，
分别针对语音信号的局部属性、动态属性和模型的可训练性，设计了一个包含卷积、
时延和组归一化的卷积-时延深度神经网络(CT-DNN)，用于说话人特征学习。
通过最大化区分训练集中的不同说话人，分帧对网络进行训练；
将每一帧在最后一个隐藏层的输出作为帧级别的说话人特征。
实验表明，与基于概率统计的基线系统相比，本文所提出的基于特征学习的说话人识别系统
在短时测试场景下取得了更好的性能表现。

\subsubsection{说话人特征学习的推广性研究}

考虑到特征学习的目标函数并不是直接针对说话人识别任务，
因此我们需要进一步验证所学说话人特征在说话人相关任务中的通用性和普适性，
以证明该特征学习的推广性。
为此，我们从三个方面验证了说话人特征学习的推广性：
首先，通过比较特征学习方法与面向任务的“端到端”方法，验证了特征学习方法对说话人识别任务的推广性；
其次，通过将所学说话人特征应用到跨语言说话人识别中，验证了特征学习方法在跨语言场景下的推广性；
最后，通过将所学说话人特征应用到短语音说话人识别中，验证了特征学习方法在短语音场景下的推广性。

\subsubsection{基于全信息训练的说话人特征学习}
\label{sec:FIT}

通过说话人特征学习的推广性研究，我们验证了基于 CT-DNN 模型所学到的说话人特征具有很强的通用性和普适性。
然而，在研究过程中我们发现，
该特征学习方法在训练过程中只关注于说话人的类间离散度，而忽略了对说话人类内内聚性的限制，
导致所学到的说话人特征存在类内发散的问题。
为此，我们首先从 CT-DNN 模型自身出发，分析模型结构中的缺陷。
我们发现 CT-DNN 模型中的特征学习层和分类层是联合训练的。
因此，为了满足区分不同说话人的目标，模型分类层中也同样学习到了部分说话人区分性的信息；
而这些信息在特征提取时却被直接舍弃，从而导致了说话人特征的“信息泄露”。
为此，我们提出了一种基于类中心趋近的训练准则，通过迭代训练的机制，
将说话人特征向量代替模型分类层，强制使全部说话人信息集中于特征学习层，生成更具有内聚性的说话人特征。
由于模型在训练过程中充分利用了网络参数，并将训练数据中的说话人信息全部聚焦在说话人特征学习中，
故此称为基于全信息训练(Full-info training, FIT)的说话人特征学习。
实验表明，与基础 CT-DNN 模型相比，基于 FIT CT-DNN 模型所学到的说话人特征在不同测试条件下取得了一致的性能提升。

\subsubsection{基于音素相关训练的说话人特征学习}

对于 FIT CT-DNN模型，其核心思想是从模型自身的角度出发，解决模型结构中的缺陷，提升说话人特征的类内内聚性。
然而，无论是 CT-DNN 模型 还是 FIT CT-DNN 模型，其在训练过程中并没有引入任何先验知识，
这就意味着特征在学习过程中完全依赖于复杂的模型结构和大量的语音数据。
这种“盲目”的数据驱动使得模型在训练过程中极易受到各种干扰因素的影响，导致模型训练的不稳定性。
我们在研究过程中发现，语音中的发音内容信息是特征学习过程中的一个主要干扰因素。
因此，我们从学习方法的角度，受条件学习的启发，通过在模型训练中先验地引入音素条件，
提出了基于音素补偿准则的音素相关训练(Phone-aware training, PAT)方法，
以此降低了说话人特征中的音素扰动，提升了说话人特征的表征能力。
实验表明，与基础 CT-DNN 模型相比，基于 PAT CT-DNN 模型所学到的说话人特征在不同测试条件下取得了一致的性能提升。

\subsection{相关研究工作}

近年来，基于深度学习的说话人识别方法研究越来越受到关注。在本论文工作期间，一些研究团队提出了一系列基于
深度神经网络的说话人识别方法。这些方法所用的模型结构各不相同，学习目标也有所差异，但基本思路是一致的：
利用DNN强大的学习能力，将语音片段映射到一个说话人空间中，得到具有更强不变性的说话人表示。
例如，Heigold等人~\cite{heigold2016end}使用长短时记忆循环神经网络(LSTM-RNN)
直接来学习句子级的说话人表示，并以逻辑回归作为后端模型实现说话人识别打分判决。
实验表明，在超过4,000个说话人的训练数据集上，该方法实现了对i-vector系统的超越。
Zhang等人~\cite{zhang2016end}提出使用卷积神经网络(CNN)学习说话人特征，
并使用一个基于注意机制的循环神经网络作为后端模型。
上述提到的所有实验都是在文本相关任务上的，其基本思想是通过增大网络的记忆时长，
直接从原始语音特征中抽取出句子级的特征表示，并基于该特征表示设计不同的后端打分模型。
最近，Snyder等人~\cite{snyder2016deep}将上述方法迁移到文本无关的说话人识别中。
实验表明，当训练数据量足够大时(102,000个说话人)，其在文本无关任务上取得了比i-vector系统更好地结果。
近期，Li等人~\cite{li2017deep}提出了类似的方法，其综合对比了各种神经网络结构在文本无关和文本
相关上的处理能力。

上述这些方法大都是基于Variani的d-vector研究~\cite{variani2014deep}，其目的是采用复杂的后端打分模型代
替d-vector中简单的合并平均。这些模型大多采用“端到端”的学习策略，将前端的说话人特征学习和后端的打分
判决整合在一起(可视为一个“黑盒子”)，并联合优化整个系统。
尽管取得了一定成功，但这一“端到端”方法存在如下两个问题：
一是系统仅针对说话人识别任务建模，无法深入理解语音信号中说话人信息的嵌入方式，
无法为其它说话人相关任务(如说话人分割、说话人自适应)提供泛化；
二是“端到端”学习对训练数据量的需求更大，对网络参数的控制更加苛刻，容易陷入欠拟合或过拟合。

与“端到端”方法不同，我们关注于帧级别的说话人特征学习。这一学习方法比“端到端”学习具有明显优势。
第一，特征学习是帧级别学习，比句子级别的“端到端”模型学习更容易，训练更稳定；
第二，特征学习可极大地减轻后端建模的压力，只要特征具有足够强的表征能力，
则说话人识别系统只需一个简单的后端模型即可准确地实现打分判决；
第三，特征学习不针对具体任务，学习得到的特征可以广泛应用于说话人相关的各项任务中，
例如，说话人聚类、说话人分割、说话人自适应和说话人转换等；
第四，说话人特征的探索可以帮助人们更深刻地理解语音信号中的信息融合方式，
特别是说话人因子和发音内容等因子之间的相互关系。

\section{论文组织结构}

本文一共包括六章内容，其具体安排如下：

第一章绪论部分。首先介绍了说话人识别的基本概念及其应用挑战；
然后综述了说话人识别的研究现状和基于深度神经网络的特征学习，
引出了本文的研究目标：说话人识别中的特征学习；
接着分析了说话人特征学习的研究难点；
最后阐述了本文的总体研究思路和相关研究内容。

第二章基于卷积-时延深度神经网络的说话人特征学习。
首先介绍了语音信号基本特性以及说话人信息在语音信号中的表征形式；
而后考虑到语音信号的局部属性、动态属性和模型的可训练性，
设计了一个包含卷积、时延和组归一化的卷积-时延深度神经网络(CT-DNN)模型，用于说话人特征学习；
最后通过定性和定量分析，验证了所学说话人特征具有很强的说话人区分性。

第三章说话人特征学习的推广性研究。
从三个角度，设计了不同的推广性研究方案，
进一步检验所学说话人特征在说话人识别相关任务中的泛化能力。
首先，比较特征学习方法与面向任务的“端到端”方法，验证了特征学习方法对说话人识别任务的推广性；
其次，将所学说话人特征应用到跨语言说话人识别中，验证了所学说话人特征在跨语言场景下的推广性；
最后，将所学说话人特征应用到短语音说话人识别中，验证了所学说话人特征在短语音场景下的推广性。

第四章基于全信息训练的说话人特征学习。
首先呈现了所学说话人特征中的类内发散问题；
然后分析了CT-DNN模型在训练过程中潜在的“信息泄露”缺陷；
进而提出了基于类中心趋近准则的全信息训练(FIT)方法，解决了“信息泄露”和类内发散的问题；
最后通过实验验证了基于FIT CT-DNN的模型结构和训练方法的有效性。

第五章基于音素相关训练的说话人特征学习。
首先阐述了所学说话人特征受发音内容的影响而表现出的分布不稳定性；
然后分析了CT-DNN模型和FIT CT-DNN模型在训练过程中所存在的“盲目”数据驱动的缺陷；
最后受条件学习的启发，提出了基于音素相关训练(PAT)的CT-DNN模型，
使特征在学习过程中得到音素知识的先验指导，在一定程度上解决了因发音内容不同而导致的说话人特征发散问题。
此外，在PAT CT-DNN模型的基础上，我们开展了多任务协同学习和信号深度分解等相关扩展性研究。

第六章总结与展望。
总结了本文的主要研究工作和研究成果，同时对相关领域的研究工作提出展望。
