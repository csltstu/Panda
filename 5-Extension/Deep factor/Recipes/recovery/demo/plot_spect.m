figure;
subplot(231);
x=load('data\emotion.ark');
x=x.';
tt = length(x(1,:));
ff = length(x(:,1));

time = [0: 1: tt];
frequency = [0: 1: ff];
imagesc(time, frequency, x);
set(gca, 'YDir', 'normal');
set(gca, 'CLim', [-5,25]);
title('Emotion');

subplot(232);
x=load(['data\speaker.ark']);
x=x.';
tt = length(x(1,:));
ff = length(x(:,1));

time = [0: 1: tt];
frequency = [0: 1: ff];
imagesc(time, frequency, x);
set(gca, 'YDir', 'normal');
set(gca, 'CLim', [-5,10]);
title('Speaker');

subplot(233);
x=load('data\phone.ark');
x=x.';
tt = length(x(1,:));
ff = length(x(:,1));

time = [0: 1: tt];
frequency = [0: ff];
imagesc(time, frequency, x);
set(gca, 'YDir', 'normal');
set(gca, 'CLim', [-5,10]);
title('Phone');

subplot(234);
x=load('data\recovery.ark');
x=x.';
tt = length(x(1,:));
ff = length(x(:,1));

time = [0: 1: tt];
frequency = [0: 1: ff];
imagesc(time, frequency, x);
set(gca, 'YDir', 'normal');
set(gca,'CLim',[-5, 25]);
title('Recovery');

subplot(235);
x=load('data\raw.ark');
x=x.';
tt = length(x(1,:));
ff = length(x(:,1));

time = [0: 1: tt];
frequency = [0: 1: ff];
imagesc(time, frequency, x);
set(gca, 'YDir', 'normal');
set(gca, 'CLim', [-5 25]);
title('Raw');
