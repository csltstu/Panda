#!/bin/bash
# Copyright 2013   Daniel Povey
#           2018   Lantian Li
# this is the spectrum recovery model, built in nnet3.

stage=0
train_stage=-10

. ./cmd.sh
. ./path.sh
. ./utils/parse_options.sh

if ! cuda-compiled; then
  cat <<EOF && exit 1
This script is intended to be used with GPUs but you have not compiled Kaldi with CUDA
If you want to use GPUs (and have them), go to src/, and configure and make on a machine
where "nvcc" is installed.
EOF
fi

epoch=15
relu_dim=1024

phone_data=phone/wsj/train
spk_data=speaker/fisher_5000/train
emo_data=exp/nnet3/tdnn_phone_spk_pi200_po40_l6_splice10/train_feats
spect_data=data/data_spect/train
dir=recovery/nnet3/dnn_re${relu}_l6_splice4

# model training
if [ $stage -le 1 ]; then
    steps/nnet3/train_triple_input_recovery.sh --stage $train_stage \
    --num-epochs $epoch --num-jobs-initial 1 --num-jobs-final 1 \
    --splice-indexes "-2,-1,0,1,2  0  0  0  0  0" \
    --feat-type raw \
    --cmvn-opts-p "--norm-means=true --norm-vars=true" \
    --cmvn-opts-s "--norm-means=false --norm-vars=false" \
    --cmvn-opts-e "--norm-means=false --norm-vars=false" \
    --cmvn-opts-t "--norm-means=false --norm-vars=false" \
    --initial-effective-lrate 0.001 --final-effective-lrate 0.0001 \
    --samples-per-iter 100000  \
    --cmd "$train_cmd" \
    --gpu-cmd "$gpu_cmd" \
    --relu-dim $relu_dim \
    --remove-egs true \
    $phone_data $spk_data $emo_data $spect_data $dir || exit 1;
fi

# decoding the phone spectrum, speaker spectrum, emotion spectrum and recovery spectrum.
if [ $stage -le 2 ]; then
  steps/make_recovery_spectrum.sh --nj 20 --cmd "$train_cmd" \
    $dir \
    phone/wsj/train \
    speaker/fisher_5000/train \
    exp/nnet3/tdnn_phone_spk_pi200_po40_l6_splice10/train_feats \
    data/data_spect/train

  steps/make_recovery_spectrum.sh --nj 20 --cmd "$train_cmd" \
    $dir \
    phone/wsj/test \
    speaker/fisher_5000/test \
    exp/nnet3/tdnn_phone_spk_pi200_po40_l6_splice10/test_feats \
    data/data_spect/test
fi

echo Done.
