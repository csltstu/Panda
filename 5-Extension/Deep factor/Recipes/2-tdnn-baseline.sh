#!/bin/bash
# Copyright 2013   Daniel Povey
#           2018   Lantian Li
# this is the d-vector baseline on emotion recognition, built in nnet3.

stage=0
train_stage=-10

. ./cmd.sh
. ./path.sh
. ./utils/parse_options.sh

if ! cuda-compiled; then
  cat <<EOF && exit 1
This script is intended to be used with GPUs but you have not compiled Kaldi with CUDA
If you want to use GPUs (and have them), go to src/, and configure and make on a machine
where "nvcc" is installed.
EOF
fi

pnorm_input=200
pnorm_output=40
train_data=data/data_fbank/train
dir=exp/nnet3/tdnn_pi${pnorm_input}_po${pnorm_output}_l6_splice10

# model training
if [ $stage -le 1 ]; then
  steps/nnet3/train_spk_vad.sh --stage $train_stage \
    --num-epochs 10 --num-jobs-initial 1 --num-jobs-final 2 \
    --splice-indexes "-4,-3,-2,-1,0,1,2,3,4  0  -2,2  0  -4,4 0" \
    --feat-type raw \
    --cmvn-opts "--norm-means=false --norm-vars=false" \
    --initial-effective-lrate 0.005 --final-effective-lrate 0.0005 \
    --samples-per-iter 100000 \
    --cmd "$train_cmd" \
    --gpu-cmd "$gpu_cmd" \
    --pnorm-input-dim $pnorm_input \
    --pnorm-output-dim $pnorm_output \
    --remove-egs true \
    $train_data exp/spk_ali $dir || exit 1;
fi

# d-vector extraction on the evaluation set.
if [ $stage -le 2 ]; then
  # confusion matrix on the training set.
  sid/extract_dvectors.sh --nj 20 --cmd "$train_cmd" $dir data/data_fbank/train $dir/train
  python local/cMatrix.py $dir/train/dvector.ark data/data_fbank/train/utt2spk_num

  # confusion matrix on the test set.
  sid/extract_dvectors.sh --nj 20 --cmd "$train_cmd" $dir data/data_fbank/test $dir/test
  python local/cMatrix.py $dir/test/dvector.ark data/data_fbank/test/utt2spk_num
fi

echo Done.
