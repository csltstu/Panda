#!/bin/bash
# Copyright 2012  Karel Vesely  Johns Hopkins University (Author: Daniel Povey)
#           2018  Lantian Li
# Apache 2.0

# Begin configuration section.
nj=4
cmd=run.pl
spect_config=conf/spect.conf
compress=true
# End configuration section.

echo "$0 $@"  # Print the command line for logging

if [ -f path.sh ]; then . ./path.sh; fi
. parse_options.sh || exit 1;

if [ $# != 3 ]; then
   echo "usage: make_spectrum.sh [options] <data-dir> <log-dir> <path-to-spectdir>";
   echo "options: "
   echo "  --spect-config <config-file>                      # config passed to compute-spectrogram-feats "
   echo "  --nj <nj>                                        # number of parallel jobs"
   echo "  --cmd (utils/run.pl|utils/queue.pl <queue opts>) # how to run jobs."
   exit 1;
fi

data=$1
logdir=$2
spectdir=$3


# make $spectdir an absolute pathname.
spectdir=`perl -e '($dir,$pwd)= @ARGV; if($dir!~m:^/:) { $dir = "$pwd/$dir"; } print $dir; ' $spectdir ${PWD}`

# use "name" as part of name of the archive.
name=`basename $data`

mkdir -p $spectdir || exit 1;
mkdir -p $logdir || exit 1;

if [ -f $data/feats.scp ]; then
  mkdir -p $data/.backup
  echo "$0: moving $data/feats.scp to $data/.backup"
  mv $data/feats.scp $data/.backup
fi

scp=$data/wav.scp

required="$scp $spect_config"

for f in $required; do
  if [ ! -f $f ]; then
    echo "make_spectrum.sh: no such file $f"
    exit 1;
  fi
done

utils/validate_data_dir.sh --no-text --no-feats $data || exit 1;

for n in $(seq $nj); do
  # the next command does nothing unless $spectdir/storage/ exists, see
  # utils/create_data_link.pl for more info.
  utils/create_data_link.pl $spectdir/raw_spect_$name.$n.ark  
done

if [ -f $data/segments ]; then
  echo "$0 [info]: segments file exists: using that."
  split_segments=""
  for n in $(seq $nj); do
    split_segments="$split_segments $logdir/segments.$n"
  done

  utils/split_scp.pl $data/segments $split_segments || exit 1;
  rm $logdir/.error 2>/dev/null

  $cmd JOB=1:$nj $logdir/make_spect_${name}.JOB.log \
    extract-segments scp,p:$scp $logdir/segments.JOB ark:- \| \
    compute-spectrogram-feats --verbose=2 --config=$spect_config ark:- ark:- \| \
    copy-feats --compress=$compress ark:- \
     ark,scp:$spectdir/raw_spect_$name.JOB.ark,$spectdir/raw_spect_$name.JOB.scp \
     || exit 1;

else
  echo "$0: [info]: no segments file exists: assuming wav.scp indexed by utterance."
  split_scps=""
  for n in $(seq $nj); do
    split_scps="$split_scps $logdir/wav.$n.scp"
  done

  utils/split_scp.pl $scp $split_scps || exit 1;
 
  $cmd JOB=1:$nj $logdir/make_spect_${name}.JOB.log \
    compute-spectrogram-feats --verbose=2 --config=$spect_config scp,p:$logdir/wav.JOB.scp ark:- \| \
    copy-feats --compress=$compress ark:- \
     ark,scp:$spectdir/raw_spect_$name.JOB.ark,$spectdir/raw_spect_$name.JOB.scp \
     || exit 1;

fi


if [ -f $logdir/.error.$name ]; then
  echo "Error producing spectrum features for $name:"
  tail $logdir/make_spect_${name}.1.log
  exit 1;
fi

# concatenate the .scp files together.
for n in $(seq $nj); do
  cat $spectdir/raw_spect_$name.$n.scp || exit 1;
done > $data/feats.scp

rm $logdir/wav.*.scp  $logdir/segments.* 2>/dev/null

nf=`cat $data/feats.scp | wc -l` 
nu=`cat $data/utt2spk | wc -l` 
if [ $nf -ne $nu ]; then
  echo "It seems not all of the feature files were successfully ($nf != $nu);"
  echo "consider using utils/fix_data_dir.sh $data"
fi

echo "Succeeded creating filterbank features for $name"
