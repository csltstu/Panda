#!/usr/bin/python

# Copyright  2018  Lantian Li
# Apache 2.0.

# this script is used to compute the confusion matrix.

import sys
import numpy as np

def fun(fin, labelf):

	fin = open(fin ,'r')
	linesf = fin.readlines()
	fin.close()

	labelf = open(labelf, 'r')
	linesl = labelf.readlines()
	labelf.close()

	utt2emodict={}
	for line in linesl:
		part = line.split()
		utt2emodict[part[0]] = int(part[1])

	cmat = np.zeros(64).reshape(8,8)
	for line in linesf:
		part = line.split()
		yonglist = [float(i) for i in part[2:-1]]
		cmat[utt2emodict[part[0]]][np.argmax(yonglist)] += 1

	allbingo = 0
	for i in range(8):
		allbingo += cmat[i][i]

	idr = float(allbingo) / np.sum(cmat)

	allnum = np.sum(cmat, 1)
	idrpart = []
	for i in range(8):
		idrpart.append(cmat[i][i] / allnum[i])

	avgidr = np.mean(idrpart)
	
	emolist = ['angry','anxious','disgust','happy','neutral','sad','surprise','worried']
	print 'Confuse Matrix = '
	for ll in range(8):
		out = emolist[ll].ljust(10)
		for j in range(8):
			out = out + str(int(cmat[ll][j])).ljust(5)
		print out
	print 'The IDR = ' + str(idr)
	print 'The AvgIDR = ' + str(avgidr)

if __name__ == '__main__':
	fin = sys.argv[1]
	labelf = sys.argv[2]
	fun(fin, labelf)