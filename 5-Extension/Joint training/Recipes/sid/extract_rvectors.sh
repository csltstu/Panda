#!/bin/bash
# Copyright   2018  Tsinghua University (Author: Lantian Li)
# Apache 2.0.

# this recipe is used for r-vector extraction.

nj=6
cmd="run.pl"
map=spk2utt
stage=-2

if [ -f path.sh ]; then . ./path.sh; fi
. parse_options.sh || exit 1;

if [ $# != 3 ]; then
  echo "Usage: $0 [opts] <nnet3-dir> <data-dir> <dve-dir>"
  echo "  --nj <nj>   # number of parallel jobs"
  echo "  --map       # filename of spk2utt or lan2utt"
  echo "  --cmd (utils/run.pl|utils/queue.pl <queue opts>) # how to run jobs."
  exit 1;
fi

dir=$1
data=$2
dvec=$3

# check data.
for f in $data/feats.scp $data/vad.scp $data/$map ; do
  [ ! -f $f ] && echo "No such file $f" && exit 1;
done

sdata=$data/split$nj 
utils/split_data.sh $data $nj

# check nnet.
if [ $stage -le -2 ]; then
  if [ ! -f $dir/final.hid_out ]; then
    nnet3-copy --binary=false $dir/final.raw $dir/final.hid_out
    sed -i "s/input=Offset(Final_log_softmax, 5)/input=Lstm1_rp_t/g" $dir/final.hid_out
  fi
fi

# utt-level decode.
if [ $stage -le -1 ]; then
  mkdir -p $dvec/log
  cmvn_opts=$(cat $dir/cmvn_opts)
  echo $cmvn_opts
  feats="ark:copy-feats scp:$sdata/JOB/feats.scp ark:- | apply-cmvn $cmvn_opts --utt2spk=ark:$sdata/JOB/utt2spk scp:$sdata/JOB/cmvn.scp ark:- ark:- | select-voiced-frames ark:- scp:$sdata/JOB/vad.scp ark:- |"

  echo $nj
  $cmd JOB=1:$nj $dvec/log/decode.JOB.log \
    nnet3-compute --use-gpu=no --extra-left-context=40 --extra-right-context=0 --frames-per-chunk=20 $dir/final.hid_out "$feats" ark:- \| \
    matrix-sum-rows ark:- ark:- \| \
    ivector-normalize-length ark:- ark,scp:$dvec/rvector.JOB.ark,$dvec/rvector.JOB.scp
  wait;

  for n in $(seq $nj); do
    cat $dvec/rvector.$n.scp || exit 1;
  done > $dvec/rvector.scp

fi

# spk/lan-level rvector.
if [ $stage -le 0 ]; then
  if [ $map -eq 'spk2utt' ]; then
    $cmd $dvec/log/speaker_mean.log \
      ivector-normalize-length scp:$dvec/rvector.scp ark:- \| \
      ivector-mean ark:$data/$map ark:- ark:- ark,t:$dvec/num_utts.ark \| \
      ivector-normalize-length ark:- ark,scp:$dvec/spk_rvector.ark,$dvec/spk_rvector.scp || exit 1;
  fi
  if [ $map -eq 'lan2utt' ]; then
    $cmd $dvec/log/lang_mean.log \
      ivector-normalize-length scp:$dvec/rvector.scp ark:- \| \
      ivector-mean ark:$data/$map ark:- ark:- ark,t:$dvec/lan_utts.ark \| \
      ivector-normalize-length ark:- ark,scp:$dvec/lan_rvector.ark,$dvec/lan_rvector.scp || exit 1;
  fi
fi

echo "Created decoding."