#!/usr/bin/python

# Copyright  2018  Lantian Li
# Apache 2.0.

import sys

def calIDR(filename):
	fin = open(filename, 'r')
	lines = fin.readlines()
	fin.close()
	error = 0
	for line in lines:
		target = line.strip().split()[0]
		nontarget = line.strip().split()[1]
		if float(target) < float(nontarget):
			error += 1	
	return error, len(lines)

if __name__ =="__main__":
	fin_1 = sys.argv[1]
	fin_2 = sys.argv[2]
	error_1, count_1 = calIDR(fin_1)
	error_2, count_2 = calIDR(fin_2)
	print "IDE:", error_1 + error_2
	print 'IDR: %.2f%%' %(100.0*(error_1 + error_2) / (count_1 + count_2))
