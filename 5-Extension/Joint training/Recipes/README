# Copyright 2018   Lantian Li

This work is an extension of '1-基础模型' to study collaborative joint training for SRE and LRE.
 
1. Clone Kaldi of branch 5.0
   git clone -b 5.0 https://github.com/kaldi-asr/kaldi.git.

2. Install Kaldi speech recognition toolkit
   More details please refer to INSTALL.

3. Build the project
   mkdir -p joint
   cd joint
   ln -s $KALDI/egs/wsj/s5/steps ./
   ln -s $KALDI/egs/wsj/s5/utils ./
   ln -s $KALDI/egs/sre08/v1/local ./
   ln -s $KALDI/egs/sre08/v1/sid ./

4. Copy src/nnet3bin/* to $KALDI/src/nnet3bin/, and compile it.
   For instance:
   cd nnet3bin
   make depend
   make
   
5. Data preparation
   In order to reproduce experiments more conveniently, here I provide all the data lists in data/, 
   more details as shown below:
   (1). data_fbank: feature extraction on Fbanks, including CSLT-C200_WSJ-E200, CSLT-C100 and WSJ-E110.
   (2). data_mfcc: feature extraction on MFCCs, including CSLT-C200_WSJ-E200, CSLT-C100 and WSJ-E110.
   (3). trials: all the test trials.
        -- speaker: for SRE evaluation.
           -- C100.trl: test trial on CSLT-C100.
           -- E110.trl: test trial on WSJ-E110.     
        -- language: for LRE evaluation.
           -- china_china.trl: enroll in Chinese and test in Chinese.
           -- wsj_wsj.trl:     enroll in English and test in English.
           -- china_wsj.trl:   enroll in Chinese and test in English.
           -- wsj_china.trl:   enroll in English and test in Chinese.
           
   If the data lists are prepared, you can run the '1-feats.sh' to implement the feature extraction.

6. Recipe of i-vector on SRE and LRE baseline
   '2-ivector.sh' is the recipe of i-vector baseline on SRE and LRE, including model training and evaluation.

7. Recipe of r-vector on SRE baseline
   '3-rvector-spk.sh' is the recipe of r-vector baseline on SRE, including model training and evaluation.

8. Recipe of r-vector on LRE baseline
   '3-rvector-lan.sh' is the recipe of r-vector baseline on LRE, including model training and evaluation.

9. Recipe of joint training on SRE and LRE
   '4-joint.sh' is the recipe of r-vector joint model on SRE and LRE, including model training and evaluation.

Note: In order to reproduce experimental results more conveniently, here I provide the i-vector/r-vector/joint model in related 'exp-dir'.
