#!/bin/bash
# Copyright 2013   Daniel Povey
#           2014   David Snyder
#           2018   Lantian Li
# this is the standard r-vector system based on LSTM, built in nnet3.

stage=0
train_stage=-10
common_egs_dir=

# LSTM options
splice_indexes="-5,-4,-3,-2,-1,0,1,2,3,4,5"
lstm_delay=" -1 "
label_delay=5
num_lstm_layers=1
cell_dim=1024
hidden_dim=1024
recurrent_projection_dim=100
non_recurrent_projection_dim=100
chunk_width=20
chunk_left_context=40
chunk_right_context=0

# training options
num_epochs=10
initial_effective_lrate=0.0006
final_effective_lrate=0.00006
num_jobs_initial=2
num_jobs_final=4
momentum=0.5
num_chunk_per_minibatch=100
samples_per_iter=20000
remove_egs=true

#decode options
extra_left_context=
extra_right_context=
frames_per_chunk=

#End configuration section

echo "$0 $@" # Print the command line for logging

. ./cmd.sh
. ./path.sh
. ./utils/parse_options.sh

if ! cuda-compiled; then
  cat <<EOF && exit 1
This script is intended to be used with GPUs but you have not compiled Kaldi with CUDA
If you want to use GPUs (and have them), go to src/, and configure and make on a machine
where "nvcc" is installed.
EOF
fi

data=CSLT-C200_WSJ-E200    # data-dir of the training set.
train_data=data/data_fbank/$data

# LSTM (r-vector) on speaker recognition.
dir=exp_rvector/$data/lstm_spk
# prepare alignment
if [ $stage -le 0 ]; then
  # generate utt2spk_num
  python local/genNum.py $train_data utt2spk
  # generate feats.len
  feat-to-len scp:$train_data/feats.scp ark,t:$train_data/feats.len
  # generate vad.ark
  copy-vector scp:$train_data/vad.scp ark,t:$train_data/vad.ark
  # generate ali.ark, spk_counts and spk_num
  mkdir -p exp_rvector/$data/spk_ali
  python local/preAli.py $train_data exp_rvector/$data/spk_ali
fi

# LSTM training for speaker recognition
if [ $stage -le 1 ]; then
  steps/nnet3/lstm/train_vad.sh --stage $train_stage \
    --label-delay $label_delay \
    --lstm-delay "$lstm_delay" \
    --num-epochs $num_epochs --num-jobs-initial $num_jobs_initial --num-jobs-final $num_jobs_final \
    --num-chunk-per-minibatch $num_chunk_per_minibatch \
    --samples-per-iter $samples_per_iter \
    --splice-indexes "$splice_indexes" \
    --feat-type raw \
    --cmvn-opts "--norm-means=false --norm-vars=false" \
    --initial-effective-lrate $initial_effective_lrate --final-effective-lrate $final_effective_lrate \
    --momentum $momentum \
    --cmd "$cuda_cmd" \
    --num-lstm-layers $num_lstm_layers \
    --cell-dim $cell_dim \
    --hidden-dim $hidden_dim \
    --recurrent-projection-dim $recurrent_projection_dim \
    --non-recurrent-projection-dim $non_recurrent_projection_dim \
    --chunk-width $chunk_width \
    --chunk-left-context $chunk_left_context \
    --chunk-right-context $chunk_right_context \
    --egs-dir "$common_egs_dir" \
    --remove-egs $remove_egs \
    $train_data exp_rvector/$data/spk_ali $dir || exit 1;
fi

# LDA/PLDA training for speaker recognition
if [ $stage -le 2 ]; then
  # d-vector extraction on the training set.
  sid/extract_rvectors.sh --nj 20 --cmd "$train_cmd" --map spk2utt \
    $dir $train_data $dir/$data

  # compute LDA transform
  ivector-compute-lda --dim=$clda  --total-covariance-factor=0.1 \
    "ark:ivector-normalize-length scp:$dir/$data/rvector.scp ark:- |" \
    ark:$train_data/utt2spk \
    $dir/$data/transform_${clda}.mat

  # compute PLDA transform
  ivector-compute-plda ark:$train_data/spk2utt \
    "ark:ivector-normalize-length scp:$dir/$data/rvector.scp ark:- |" \
    $dir/$data/plda 2>$dir/$data/log/plda.log
fi


# r-vector extraction on the evaluation set.
if [ $stage -le 3 ]; then
  lang=data/data_fbank/CSLT-C100
  for sub in model_C100 test_C100 ; do
    sid/extract_rvectors.sh --nj 20 --cmd "$train_cmd" --map spk2utt \
      $dir $lang/$sub $dir/$sub
  done

  lang=data/data_fbank/WSJ-E110
  for sub in model_E110 test_E110 ; do
    sid/extract_rvectors.sh --nj 20 --cmd "$train_cmd" --map spk2utt \
      $dir $lang/$sub $dir/$sub
  done
fi

# r-vector evaluation on speaker recognition
if [ $stage -le 4 ]; then
  echo china
  local/evaluate_rv_spk.sh $dir/$data \
    $dir/model_C100 \
    $dir/test_C100 \
    data/trials/speaker/C100.trl

  echo wsj
  local/evaluate_rv_spk.sh $dir/$data \
    $dir/model_E110 \
    $dir/test_E110 \
    data/trials/speaker/E110.trl
fi
