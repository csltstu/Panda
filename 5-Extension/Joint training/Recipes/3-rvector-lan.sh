#!/bin/bash
# Copyright 2013   Daniel Povey
#           2014   David Snyder
#           2018   Lantian Li
# this is the standard r-vector system based on LSTM, built in nnet3.

stage=0
train_stage=-10
common_egs_dir=

# LSTM options
splice_indexes="-5,-4,-3,-2,-1,0,1,2,3,4,5"
lstm_delay=" -1 "
label_delay=5
num_lstm_layers=1
cell_dim=1024
hidden_dim=1024
recurrent_projection_dim=100
non_recurrent_projection_dim=100
chunk_width=20
chunk_left_context=40
chunk_right_context=0

# training options
num_epochs=10
initial_effective_lrate=0.0006
final_effective_lrate=0.00006
num_jobs_initial=2
num_jobs_final=4
momentum=0.5
num_chunk_per_minibatch=100
samples_per_iter=20000
remove_egs=true

#decode options
extra_left_context=
extra_right_context=
frames_per_chunk=

#End configuration section

echo "$0 $@" # Print the command line for logging

. ./cmd.sh
. ./path.sh
. ./utils/parse_options.sh

if ! cuda-compiled; then
  cat <<EOF && exit 1
This script is intended to be used with GPUs but you have not compiled Kaldi with CUDA
If you want to use GPUs (and have them), go to src/, and configure and make on a machine
where "nvcc" is installed.
EOF
fi

data=CSLT-C200_WSJ-E200    # data-dir of the training set.
train_data=data/data_fbank/$data

# LSTM (r-vector) on language recognition.
dir=exp_rvector/$data/lstm_lan
# prepare alignment
if [ $stage -le 0 ]; then
  # generate utt2spk_num
  python local/genNum.py $train_data utt2lan
  # generate feats.len
  feat-to-len scp:$train_data/feats.scp ark,t:$train_data/feats.len
  # generate vad.ark
  copy-vector scp:$train_data/vad.scp ark,t:$train_data/vad.ark
  # generate ali.ark, spk_counts and spk_num
  mkdir -p exp_rvector/$data/lan_ali
  python local/preAli.py $train_data exp_rvector/$data/lan_ali
fi

# LSTM training for language recognition
if [ $stage -le 1 ]; then
  steps/nnet3/lstm/train_vad.sh --stage $train_stage \
    --label-delay $label_delay \
    --lstm-delay "$lstm_delay" \
    --num-epochs $num_epochs --num-jobs-initial $num_jobs_initial --num-jobs-final $num_jobs_final \
    --num-chunk-per-minibatch $num_chunk_per_minibatch \
    --samples-per-iter $samples_per_iter \
    --splice-indexes "$splice_indexes" \
    --feat-type raw \
    --cmvn-opts "--norm-means=false --norm-vars=false" \
    --initial-effective-lrate $initial_effective_lrate --final-effective-lrate $final_effective_lrate \
    --momentum $momentum \
    --cmd "$cuda_cmd" \
    --num-lstm-layers $num_lstm_layers \
    --cell-dim $cell_dim \
    --hidden-dim $hidden_dim \
    --recurrent-projection-dim $recurrent_projection_dim \
    --non-recurrent-projection-dim $non_recurrent_projection_dim \
    --chunk-width $chunk_width \
    --chunk-left-context $chunk_left_context \
    --chunk-right-context $chunk_right_context \
    --egs-dir "$common_egs_dir" \
    --remove-egs $remove_egs \
    $train_data exp_rvector/$data/lan_ali $dir || exit 1;
fi

# r-vector extraction on the evaluation set.
if [ $stage -le 3 ]; then
  sid/extract_rvectors.sh --nj 20 --cmd "$train_cmd" --map lan2utt \
    $dir $train_data $dir/$data

  lang=data/data_fbank/CSLT-C100
  for sub in model_C100 test_C100 ; do
    sid/extract_rvectors.sh --nj 20 --cmd "$train_cmd" --map lan2utt \
      $dir $lang/$sub $dir/$sub
  done

  lang=data/data_fbank/WSJ-E110
  for sub in model_E110 test_E110 ; do
    sid/extract_rvectors.sh --nj 20 --cmd "$train_cmd" --map lan2utt \
      $dir $lang/$sub $dir/$sub
  done
fi

# r-vector evaluation on language recognition with cosine metric
if [ $stage -le 4 ]; then
  echo china
  local/evaluate_rv_lan.sh \
    $dir/model_C100 \
    $dir/model_E110 \
    $dir/test_C100 \
    data/trials/language/china_china.trl
    data/trials/language/wsj_china.trl

  paste foo_cosine_lan1 foo_cosine_lan2 | awk '{print $3, $6}' > foo_cosine_china
  rm foo_cosine_lan1 foo_cosine_lan2

  echo wsj
  local/evaluate_rv_lan.sh \
    $dir/model_E110 \
    $dir/model_C100 \
    $dir/test_E110 \
    data/trials/language/wsj_wsj.trl
    data/trials/language/china_wsj.trl

  paste foo_cosine_lan1 foo_cosine_lan2 | awk '{print $3, $6}' > foo_cosine_wsj
  rm foo_cosine_lan1 foo_cosine_lan2
  # compute IDE and IDR.
  python local/genIDR.py foo_cosine_china foo_cosine_wsj
fi

# r-vector evaluation on language recognition with linear SVM
if [ $stage -le 5 ]; then
  path=exp_rvector/$data/svm
  mkdir -p $path/dev $path/test
  # dev
  ivector-normalize-length scp:$dir/$data/rvector.scp ark,t:$path/dev/dev.ark
  python local/dataFormat.py $path/dev/dev.ark $path/dev/dev.dat
  # test
  ivector-normalize-length scp:$dir/test_C100/rvector.scp ark,t:$path/test/C100.ark
  python local/dataFormat.py $path/test/C100.ark $path/test/C100.dat
  ivector-normalize-length scp:$dir/test_E110/rvector.scp ark,t:$path/test/E110.ark
  python local/dataFormat.py $path/test/E110.ark $path/test/E110.dat
  cat $path/test/C100.dat $path/test/E110.dat > $path/test/test.dat
  # SVM training and evaluation.
  python steps/make_svm.py $path/dev/dev.dat $path/test/test.dat
fi

# softmax outputs of LSTM on the evaluation set.
if [ $stage -le 6 ]; then
  lang=data/data_fbank/CSLT-C100/test_C100
  sub=`basename $lang`
  sid/extract_softmax.sh --nj 20 --cmd "$train_cmd" \
    $dir $lang/$sub $dir/${sub}_softmax
  copy-vector scp:$dir/${sub}_softmax/rvector.scp ark,t:tmp
  awk '{print $4, $3}' tmp > foo_softmax_china
  rm tmp

  lang=data/data_fbank/WSJ-E110/test_E110
  sub=`basename $lang`
  sid/extract_softmax.sh --nj 20 --cmd "$train_cmd" \
    $dir $lang/$sub $dir/${sub}_softmax
  copy-vector scp:$dir/${sub}_softmax/rvector.scp ark,t:tmp
  awk '{print $3, $4}' tmp > foo_softmax_wsj
  rm tmp

  # compute IDE and IDR.
  python local/genIDR.py foo_softmax_china foo_softmax_wsj
fi