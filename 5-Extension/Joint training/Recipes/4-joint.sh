#!/bin/bash
# Copyright 2013   Daniel Povey
#           2014   David Snyder
#           2018   Lantian Li
# this is the standard r-vector system based on LSTM, built in nnet3.

stage=0
train_stage=-10
common_egs_dir=

# LSTM options
splice_indexes="-5,-4,-3,-2,-1,0,1,2,3,4,5"
lstm_delay=" -1 "
label_delay=5
num_lstm_layers=1
cell_dim=1024
hidden_dim=1024
recurrent_projection_dim=100
non_recurrent_projection_dim=100
chunk_width=20
chunk_left_context=40
chunk_right_context=0

# training options
num_epochs=10
initial_effective_lrate=0.0006
final_effective_lrate=0.00006
num_jobs_initial=2
num_jobs_final=4
momentum=0.5
num_chunk_per_minibatch=100
samples_per_iter=20000
remove_egs=true

#decode options
extra_left_context=
extra_right_context=
frames_per_chunk=

#End configuration section

echo "$0 $@" # Print the command line for logging

. ./cmd.sh
. ./path.sh
. ./utils/parse_options.sh

if ! cuda-compiled; then
  cat <<EOF && exit 1
This script is intended to be used with GPUs but you have not compiled Kaldi with CUDA
If you want to use GPUs (and have them), go to src/, and configure and make on a machine
where "nvcc" is installed.
EOF
fi

gate=f    # gate of joint training, e.g., i, f, o, g, ifog.
data=CSLT-C200_WSJ-E200    # data-dir of the training set.
train_data=data/data_fbank/$data
dir=exp_joint/nnet3/lstm_joint_${gate}_200
spk_ali=exp_rvector/$data/spk_ali   # more details refer to 3-rvector-spk.sh
lan_ali=exp_rvector/$data/lan_ali   # more details refer to 3-rvector-lan.sh

# Joint training for SRE and LRE
if [ $stage -le 0 ]; then
  steps/nnet3/lstm/train_spk_lan_vad.sh --stage $train_stage \
    --label-delay $label_delay \
    --lstm-delay "$lstm_delay" \
    --num-epochs $num_epochs --num-jobs-initial $num_jobs_initial --num-jobs-final $num_jobs_final \
    --num-chunk-per-minibatch $num_chunk_per_minibatch \
    --samples-per-iter $samples_per_iter \
    --splice-indexes "$splice_indexes" \
    --feat-type raw \
    --cmvn-opts "--norm-means=false --norm-vars=false" \
    --initial-effective-lrate $initial_effective_lrate --final-effective-lrate $final_effective_lrate \
    --momentum $momentum \
    --cmd "$cuda_cmd" \
    --num-lstm-layers $num_lstm_layers \
    --cell-dim $cell_dim \
    --hidden-dim $hidden_dim \
    --recurrent-projection-dim $recurrent_projection_dim \
    --non-recurrent-projection-dim $non_recurrent_projection_dim \
    --chunk-width $chunk_width \
    --chunk-left-context $chunk_left_context \
    --chunk-right-context $chunk_right_context \
    --egs-dir "$common_egs_dir" \
    --remove-egs $remove_egs \
    $train_data $spk_ali $lan_ali $dir || exit 1;
fi


# LDA/PLDA training for speaker recognition
if [ $stage -le 2 ]; then
  # d-vector extraction on the training set.
  sid/extract_rvectors_joint_spk.sh --nj 20 --cmd "$train_cmd" \
    $dir $train_data $dir/${data}_spk

  # compute LDA transform
  ivector-compute-lda --dim=$clda  --total-covariance-factor=0.1 \
    "ark:ivector-normalize-length scp:$dir/${data}_spk/rvector.scp ark:- |" \
    ark:$train_data/utt2spk \
    $dir/${data}_spk/transform_${clda}.mat

  # compute PLDA transform
  ivector-compute-plda ark:$train_data/spk2utt \
    "ark:ivector-normalize-length scp:$dir/${data}_spk/rvector.scp ark:- |" \
    $dir/${data}_spk/plda 2>$dir/${data}_spk/log/plda.log
fi


# r-vector extraction on speaker recognition
if [ $stage -le 3 ]; then
  lang=data/data_fbank/CSLT-C100
  for sub in model_C100 test_C100 ; do
    sid/extract_rvectors_joint_spk.sh --nj 20 --cmd "$train_cmd" --map spk2utt \
      $dir $lang/$sub $dir/${sub}_spk
  done

  lang=data/data_fbank/WSJ-E110
  for sub in model_E110 test_E110 ; do
    sid/extract_rvectors_joint_spk.sh --nj 20 --cmd "$train_cmd" --map spk2utt \
      $dir $lang/$sub $dir/${sub}_spk
  done
fi

# r-vector evaluation on speaker recognition
if [ $stage -le 4 ]; then
  echo china
  local/evaluate_rv_spk.sh $dir/${data}_spk \
    $dir/model_C100_spk \
    $dir/test_C100_spk \
    data/trials/speaker/C100.trl

  echo wsj
  local/evaluate_rv_spk.sh $dir/${data}_spk \
    $dir/model_E110_spk \
    $dir/test_E110_spk \
    data/trials/speaker/E110.trl
fi


# r-vector extraction on language recognition
if [ $stage -le 3 ]; then
  sid/extract_rvectors_joint_lan.sh --nj 20 --cmd "$train_cmd" \
    $dir $train_data $dir/${data}_lan

  lang=data/data_fbank/CSLT-C100
  for sub in model_C100 test_C100 ; do
    sid/extract_rvectors_joint_lan.sh --nj 20 --cmd "$train_cmd" \
      $dir $lang/$sub $dir/${sub}_lan
  done

  lang=data/data_fbank/WSJ-E110
  for sub in model_E110 test_E110 ; do
    sid/extract_rvectors_joint_lan.sh --nj 20 --cmd "$train_cmd" \
      $dir $lang/$sub $dir/${sub}_lan
  done
fi

# r-vector evaluation on language recognition with cosine metric
if [ $stage -le 4 ]; then
  echo china
  local/evaluate_rv_lan.sh \
    $dir/model_C100_lan \
    $dir/model_E110_lan \
    $dir/test_C100_lan \
    data/trials/language/china_china.trl
    data/trials/language/wsj_china.trl

  paste foo_cosine_lan1 foo_cosine_lan2 | awk '{print $3, $6}' > foo_cosine_china
  rm foo_cosine_lan1 foo_cosine_lan2

  echo wsj
  local/evaluate_rv_lan.sh \
    $dir/model_E110_lan \
    $dir/model_C100_lan \
    $dir/test_E110_lan \
    data/trials/language/wsj_wsj.trl
    data/trials/language/china_wsj.trl

  paste foo_cosine_lan1 foo_cosine_lan2 | awk '{print $3, $6}' > foo_cosine_wsj
  rm foo_cosine_lan1 foo_cosine_lan2
  # compute IDE and IDR.
  python local/genIDR.py foo_cosine_china foo_cosine_wsj
fi

# r-vector evaluation on language recognition with linear SVM
if [ $stage -le 5 ]; then
  path=exp_rvector/$data/svm
  mkdir -p $path/dev $path/test
  # dev
  ivector-normalize-length scp:$dir/${data}_lan/rvector.scp ark,t:$path/dev/dev.ark
  python local/dataFormat.py $path/dev/dev.ark $path/dev/dev.dat
  # test
  ivector-normalize-length scp:$dir/test_C100_lan/rvector.scp ark,t:$path/test/C100.ark
  python local/dataFormat.py $path/test/C100.ark $path/test/C100.dat
  ivector-normalize-length scp:$dir/test_E110_lan/rvector.scp ark,t:$path/test/E110.ark
  python local/dataFormat.py $path/test/E110.ark $path/test/E110.dat
  cat $path/test/C100.dat $path/test/E110.dat > $path/test/test.dat
  # SVM training and evaluation.
  python steps/make_svm.py $path/dev/dev.dat $path/test/test.dat
fi

# softmax outputs of LSTM on language recognition
if [ $stage -le 6 ]; then
  lang=data/data_fbank/CSLT-C100/test_C100
  sub=`basename $lang`
  sid/extract_softmax_joint_lan.sh --nj 20 --cmd "$train_cmd" \
    $dir $lang/$sub $dir/${sub}_lan_softmax
  copy-vector scp:$dir/${sub}_lan_softmax/rvector.scp ark,t:tmp
  awk '{print $4, $3}' tmp > foo_softmax_china
  rm tmp

  lang=data/data_fbank/WSJ-E110/test_E110
  sub=`basename $lang`
  sid/extract_softmax_joint_lan.sh --nj 20 --cmd "$train_cmd" \
    $dir $lang/$sub $dir/${sub}_lan_softmax
  copy-vector scp:$dir/${sub}_lan_softmax/rvector.scp ark,t:tmp
  awk '{print $3, $4}' tmp > foo_softmax_wsj
  rm tmp

  # compute IDE and IDR.
  python local/genIDR.py foo_softmax_china foo_softmax_wsj
fi
